﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetUserRestrictionQuery : IRequest<bool>
    {
        public int TelegramUserId { get; private set; }

        public GetUserRestrictionQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }

    public class GetUserRestrictionQueryHandler : AsyncRequestHandler<GetUserRestrictionQuery, bool>
    {
        IUserRepository userRepository;
        public GetUserRestrictionQueryHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        protected override async Task<bool> HandleCore(GetUserRestrictionQuery request)
        {
            try
            {
                var user = userRepository.GetUser(request.TelegramUserId);
                return user.IsRestricted;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
