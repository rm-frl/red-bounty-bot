﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Models
{
    public class CalculatedPointsDTO
    {
        public int InGroupPoints { get; set; }
        public int TwitterPoints { get; set; }
        public int FacebookPoints { get; set; }
        public int ReferalsPoints { get; set; }
        public int ReferalsCount { get; set; }

        public int TotalPoints => InGroupPoints + TwitterPoints + FacebookPoints + ReferalsPoints;
    }
}
