﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class ShowMainMenuState: BaseState
    {
        public ShowMainMenuState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitMainMenuState(Dialog));
        }

        public ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(MainMenuDialog.CREATE_PROFILE_LABEL),
                },
                 new KeyboardButton[]{
                    new KeyboardButton(MainMenuDialog.SHOW_DETAILS_LABEL),
                },
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = "Main Menu" } },
                ReplyMarkup = keyboard
            };
            return reply;
        }
    }
}
