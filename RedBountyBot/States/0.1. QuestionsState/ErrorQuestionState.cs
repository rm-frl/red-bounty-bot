﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Dialogs;

namespace RedBountyBot.States
{
    public class ErrorQuestionState : BaseState
    {
        BaseQuestionDialog dialog;

        public ErrorQuestionState(BaseQuestionDialog dialog) : base(dialog)
        {
            this.dialog = dialog;
        }

        public override void OnEntry()
        {
            dialog.Notify(this, dialog.ErrorMessage);
            Dialog.ChangeState(new WaitQuestionState(dialog));
        }
    }
}
