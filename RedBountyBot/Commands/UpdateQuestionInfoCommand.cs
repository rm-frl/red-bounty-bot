﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateQuestionInfoCommand: IRequest
    {
        public int TelegramUserId { get; private set; }
        public string LastQuestionClassName { get; set; } = null;
        public bool? IsAllQuestionsAnswered { get; set; } = null;
        public int? NumberAttempts { get; set; } = null;
        public string RandomQuestionIndexes { get; set; } = null;
        public int? NumberAnsweredQuestions { get; set; } = null;

        public UpdateQuestionInfoCommand(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }

    public class UpdateQuestionsInfoCommandHandler : AsyncRequestHandler<UpdateQuestionInfoCommand>
    {
        IUserInfoRepository userInfoRepository;

        public UpdateQuestionsInfoCommandHandler(IUserInfoRepository userInfoRepository)
        {
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task HandleCore(UpdateQuestionInfoCommand request)
        {
            var userInfo = userInfoRepository.GetInfo(request.TelegramUserId);

            try
            {
                if (request.LastQuestionClassName != null)
                    userInfo.LastQuestionClassName = request.LastQuestionClassName;
                if (request.IsAllQuestionsAnswered != null)
                    userInfo.IsAllQuestionsAnswered = (bool)request.IsAllQuestionsAnswered;
                if (request.NumberAttempts != null)
                    userInfo.NumberAttempts = (int)request.NumberAttempts;
                if (request.RandomQuestionIndexes != null)
                    userInfo.RandomQuestionIndexes = request.RandomQuestionIndexes;
                if (request.NumberAnsweredQuestions != null)
                    userInfo.NumberAnsweredQuestions = (int)request.NumberAnsweredQuestions;

                userInfoRepository.UpdateInfo(userInfo);
            }
            catch (Exception e) { }
        }
    }
}