﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RedBountyBot.Models
{
    public class TelegramUserInfo
    {
        [Key]
        public int Id { get; set; }

        public int TelegramUserId { get; set; }

        public string Email { get; set; }

        public string TwitterScreenName { get; set; }

        public long? TwitterUserId { get; set; }

        public bool TwitterIsFollowedBy { get; set; }

        public string FacebookProfile { get; set; }

        public string EthereumAddress { get; set; }

        public int TotalPoints { get; set; }

        public string Question1 { get; set; }

        public string Question2 { get; set; }

        public string Question3 { get; set; }

        public string LastQuestionClassName { get; set; }

        public bool IsAllQuestionsAnswered { get; set; }

        public int NumberAttempts { get; set; }

        public string RandomQuestionIndexes { get; set; }

        public int NumberAnsweredQuestions { get; set; }

        [ForeignKey("TelegramUserId")]
        public TelegramUser User { get; set; }
    }
}
