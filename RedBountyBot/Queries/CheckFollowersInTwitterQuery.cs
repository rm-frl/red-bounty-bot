﻿using MediatR;
using RedBountyBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class CheckFollowersInTwitterQuery: IRequest<bool>
    {
        public string UserName { get; set; }

        public CheckFollowersInTwitterQuery(string userName)
        {
            UserName = userName;
        }
    }

    public class CheckFollowersInTwitterQueryHandler : AsyncRequestHandler<CheckFollowersInTwitterQuery, bool>
    {
        ITwitterService twitterService;

        public CheckFollowersInTwitterQueryHandler(ITwitterService twitterService)
        {
            this.twitterService = twitterService;
        }

        protected override async Task<bool> HandleCore(CheckFollowersInTwitterQuery request)
        {
            return twitterService.IsFollowUser(request.UserName, twitterService.TargetGroupName);
        }
    }

}
