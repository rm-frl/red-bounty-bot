﻿using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using RedBountyBot.Queries;

namespace RedBountyBot.Dialogs
{
    public class EmailDialog : BaseDialog
    {
        public EmailDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage => "Please enter your Email address.";
        public override string ErrorTextMessage => "Sorry! That was an invalid email address. Please enter the correct email address.";
        public override string SuccessTextMessage => "Thank you! We have recorded your email address.";

        public override bool IsValidMessage(Update update)
        {
            if (update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage
                && update.Message.Text.Contains("@"))
            {
                string column = nameof(TelegramUserInfo.Email);
                string value = update.Message.Text;
                var checkDuplicatesInDatabaseQuery = new CheckDuplicatesInDatabaseQuery(this.TelegramUserId, column, value);
                var containsAny = Mediator.Send(checkDuplicatesInDatabaseQuery).Result;
                if (!containsAny)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
