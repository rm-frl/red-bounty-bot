﻿using MediatR;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Commands
{
    public class SendReplyTextCommand: IRequest<bool>
    {
        public int UserId { get; private set; }

        public ReplyTextMessage Reply { get; private set; }

        public SendReplyTextCommand(int userId, ReplyTextMessage reply)
        {
            UserId = userId;
            Reply = reply;
        }
    }

    public class SendReplyTextCommandHandler : AsyncRequestHandler<SendReplyTextCommand, bool>
    {
        IClientService telegramService;

        public SendReplyTextCommandHandler(IClientService telegramService)
        {
            this.telegramService = telegramService;
        }

        protected override async Task<bool> HandleCore(SendReplyTextCommand request)
        {
            string message = "null";
            if (request.Reply.Update != null && request.Reply.Update.Message != null)
                message = request.Reply.Update.Message.Text;
            try
            {
                await telegramService.Client.SendTextMessageAsync(
                        chatId: request.UserId,
                        text: message,
                        parseMode: request.Reply.ParseMode,
                        disableWebPagePreview: request.Reply.DisableWebPageView,
                        replyMarkup: request.Reply.ReplyMarkup
                        );
            }
            catch (Exception e)
            {

            }
            return true;
        }
    }

}
