﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    /// <summary>
    /// Save TwitterId and IsFollowedBy Main Group
    /// </summary>
    public class SaveTwitterInfoCommand: IRequest
    {
        public int TelegramUserId { get; private set; }
        public string ScreeName { get; private set; }

        public SaveTwitterInfoCommand(string screenName, int telegramUserId)
        {
            TelegramUserId = telegramUserId;
            ScreeName = screenName;
        }
    }

    public class SaveTwitterInfoCommandHandler : AsyncRequestHandler<SaveTwitterInfoCommand>
    {
        ITwitterService twitterService;
        IUserInfoRepository userInfoRepository;

        public SaveTwitterInfoCommandHandler(IUserInfoRepository userInfoRepository, ITwitterService twitterService)
        {
            this.twitterService = twitterService;
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task HandleCore(SaveTwitterInfoCommand request)
        {
            var info = userInfoRepository.GetInfo(request.TelegramUserId);
            if (info == null)
            {
                info = new TelegramUserInfo()
                {
                    TelegramUserId = request.TelegramUserId,
                };
                try
                {
                    userInfoRepository.CreateInfo(info);
                }
                catch { return; }
            }

            if (!String.IsNullOrEmpty(request.ScreeName))
            {
                string screeenName = request.ScreeName;

                long? twitterUserId = twitterService.GetUserId(screeenName);

                if (twitterUserId != null)
                {
                    info.TwitterUserId = twitterUserId;
                }

                bool isFollowedBy = twitterService.IsFollowUser(screeenName, twitterService.TargetGroupName);

                info.TwitterIsFollowedBy = isFollowedBy;

                userInfoRepository.UpdateInfo(info);
            }
        }
    }
}
