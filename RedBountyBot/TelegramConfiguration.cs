﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot
{
    public class TelegramConfiguration
    {
        public string BotToken { get; set; }
        public string TargetGroupName { get; set; }
    }
}
