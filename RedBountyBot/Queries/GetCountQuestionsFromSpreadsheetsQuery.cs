﻿using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetCountQuestionFromSpreadsheetsQuery : IRequest<int>
    {
    }

    public class GetCountQuestionFromSpreadsheetsQueryHandler : AsyncRequestHandler<GetCountQuestionFromSpreadsheetsQuery, int>
    {
        IGoogleDriveService googleDriveService;

        public GetCountQuestionFromSpreadsheetsQueryHandler(IGoogleDriveService googleDriveService)
        {
            this.googleDriveService = googleDriveService;
        }

        protected override Task<int> HandleCore(GetCountQuestionFromSpreadsheetsQuery request)
        {
            return googleDriveService.GetCountQuestion();
        }
    }
}
