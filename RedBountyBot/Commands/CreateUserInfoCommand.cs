﻿using RedBountyBot.DAL;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class CreateUserInfoCommand: IRequest
    {
        public TelegramUserInfo UserInfo { get; private set; }
        public CreateUserInfoCommand(TelegramUserInfo userInfo)
        {
            UserInfo = userInfo;
        }
    }

    public class CreateUserInfoCommandHandler : AsyncRequestHandler<CreateUserInfoCommand>
    {
        IUserInfoRepository userInfoRepository;
        public CreateUserInfoCommandHandler(IUserInfoRepository userInfoRepository)
        {
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task HandleCore(CreateUserInfoCommand request)
        {
            userInfoRepository.CreateInfo(request.UserInfo);
        }
    }
}
