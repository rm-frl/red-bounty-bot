﻿using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Models;

namespace RedBountyBot.Commands
{
    public class UpdateAllTwitterStatusesInDatabaseCommand: IRequest
    {
    }

    public class UpdateAllTwitterStatusesInDatabaseCommandHandler : AsyncRequestHandler<UpdateAllTwitterStatusesInDatabaseCommand>
    {
        IUserInfoRepository userInfoRepository;
        ITwitterService twitterService;

        public UpdateAllTwitterStatusesInDatabaseCommandHandler(IUserInfoRepository userInfoRepository, ITwitterService twitterService)
        {
            this.userInfoRepository = userInfoRepository;
            this.twitterService = twitterService;
        }

        protected override async Task HandleCore(UpdateAllTwitterStatusesInDatabaseCommand request)
        {
            try
            {
                var allUsersInfo = userInfoRepository.GetAllUsersInfo();

                List<long> allTwitterIds = new List<long>();
                long? cursor = null;
                do
                {
                    var response = twitterService.Token.Followers.IdsAsync(twitterService.TargetGroupName, cursor).Result;
                    allTwitterIds.AddRange(response);
                    cursor = response.NextCursor;
                } while (cursor != 0);

                var updatedUsers = new List<TelegramUserInfo>();
                int i = 0;
                foreach (var user in allUsersInfo.Where(x => !String.IsNullOrEmpty(x.TwitterScreenName)))
                {
                    if (i != 0 && i % 900 == 0)
                    {
                        System.Threading.Thread.Sleep(900000); // twitter has limit in 900 req/15 min. 900K milisec = 15 min
                    }

                    ////////////

                    bool isFollowedBy = false;
                    long? twitterId = null;

                    if (user.TwitterUserId != null)
                    {
                        twitterId = user.TwitterUserId;
                    }
                    else
                    {
                        twitterId = twitterService.GetUserId(user.TwitterScreenName);
                        i++;
                    }

                    isFollowedBy = allTwitterIds.Contains(twitterId ?? -1);

                    user.TwitterUserId = twitterId;
                    user.TwitterIsFollowedBy = isFollowedBy;
                    userInfoRepository.UpdateInfo(user);
                }
            }
            catch(Exception e)
            {

            }
        }
    }
}
