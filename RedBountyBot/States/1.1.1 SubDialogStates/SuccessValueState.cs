﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class SuccessValueState: BaseState
    {
        public SuccessValueState(BaseDialog Dialog) : base(Dialog) { }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
        }

        public override ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = false;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Dialog.SuccessTextMessage } },
                ReplyMarkup = keyboard   
            };

            return reply;
        }
    }
}
