﻿using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.Dialogs
{
    public class FacebookDialog : BaseDialog
    {
        public FacebookDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage => "Please send us a screenshot that shows you have followed Real Estate Doc’s Facebook page (https://www.facebook.com/realestatedoc.io).";
        public override string SuccessTextMessage => "Thank you! We have uploaded your screenshot to the database!!";
        public override string ErrorTextMessage => "Sorry! That was not a picture. Please send the screenshots in picture formats only.";
        public override string UpdateTextMessage => "Please! Attach screenshot and send me";

        public override bool IsValidMessage(Update update)
        {
            return update.Message.Type == Telegram.Bot.Types.Enums.MessageType.PhotoMessage;
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
