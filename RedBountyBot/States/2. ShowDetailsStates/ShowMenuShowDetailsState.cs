﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class ShowMenuShowDetailsState: BaseState
    {
        public ShowMenuShowDetailsState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitMenuShowDetailsState(Dialog));
        }

        public override ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(ShowDetailsDialog.SHOW_MY_PROFILE_LABEL),
                },
                 new KeyboardButton[]{
                    new KeyboardButton(ShowDetailsDialog.SHOW_GROUP_INVITERS),
                },
                 new KeyboardButton[]{
                    new KeyboardButton(ShowDetailsDialog.SHOW_EARNED_POINTS),
                },
                  new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = "Show Details For You" } },
                ReplyMarkup = keyboard
            };
            return reply;
        }
    }
}
