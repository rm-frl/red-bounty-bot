﻿using RedBountyBot.DAL;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RedBountyBot.Notifications
{
    public class CreateUserInDatabaseNotificationHandler : INotificationHandler<CreateUserNotification>
    {
        IUserRepository userRepository;

        public CreateUserInDatabaseNotificationHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task Handle(CreateUserNotification notification, CancellationToken cancellationToken)
        {
             userRepository.CreateUser(notification.User);
        }
    }
}
