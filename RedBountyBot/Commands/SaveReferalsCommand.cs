﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class SaveReferalsCommand: IRequest<bool>
    {
        public int FromId { get; set; }
        public List<ReferalUser> Referals { get; set; }

        public SaveReferalsCommand(int fromId, List<ReferalUser> referals)
        {
            FromId = fromId;
            Referals = referals;
        }
    }

    public class SaveReferalsCommandHandler : AsyncRequestHandler<SaveReferalsCommand, bool>
    {
        IReferalUserRepository referalRepository;
        IGoogleDriveService googleDriveService;

        public SaveReferalsCommandHandler(IReferalUserRepository referalRepository, IGoogleDriveService googleDriveService)
        {
            this.referalRepository = referalRepository;
            this.googleDriveService = googleDriveService;
        }

        protected override async Task<bool> HandleCore(SaveReferalsCommand request)
        {
            foreach (var referal in request.Referals)
            {
                var refs = referalRepository.GetAllUserReferals(request.FromId);
                if (refs.FirstOrDefault(r => r.ReferalUserId == referal.ReferalUserId) == null)
                {
                    if (referal.Id < 0) referal.Id = 0;
                    referal.FromId = request.FromId;
                    referalRepository.AddReferal(referal);

                    //await googleDriveService.ExportUserInfo(request.FromId);
                }
            }
            return true;
        }
    }

}
