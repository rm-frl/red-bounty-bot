﻿using RedBountyBot.Commands;
using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class InitEarnedPointsState: BaseState
    {
        public InitEarnedPointsState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitEarnedPointsState(Dialog));
        }

        public override ReplyTextMessage GetReply()
        {
            var calculatePointsCommand = new CalculatePointsCommand(Dialog.TelegramUserId);
            var pointsDTO = Dialog.Mediator.Send(calculatePointsCommand).Result;

            string text = String.Format("*You’ve earned from:*\n\n"
                + String.Format("Joining our Telegram group: {0}\n", pointsDTO.InGroupPoints)
                + String.Format("Following our Twitter: {0}\n", pointsDTO.TwitterPoints)
                + String.Format("Uploading a Facebook screenshot showing that you’ve liked our page: {0}\n", pointsDTO.FacebookPoints)
                + String.Format("Referring {0} member(s): {1}\n", pointsDTO.ReferalsCount, pointsDTO.ReferalsPoints)
                + String.Format("*TOTAL*: {0} REDT tokens!\n\n", pointsDTO.TotalPoints)
                + "Please make sure you have accurately provided us your Ethereum address for us to credit in your bounty tokens promptly. We are not responsible for your error in entering in your Ethereum address that resulted in you not receiving your bounty.\n😁👍"
                );

            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                  new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                ReplyMarkup = keyboard
            };
            return reply;
        }
    }
}
