﻿using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using RedBountyBot.Models;

namespace RedBountyBot.Services
{
    public class GoogleDriveService : IGoogleDriveService
    {
        const string spreadSheetId1 = "1KKY-q7C0fIOeYeviwGvYP21IDyngNnGpvtW4tI8Bz0g";
        //const string spreadSheetId2 = "";
        const string RandomQuestionsSpreadSheetId = "1hM55KRPYehS89rcZK1yaoRQ4LEQBVOyXqWsIUgTNlLQ";

        const string range = "Sheet1";

        IUserRepository userRepository;
        IUserInfoRepository userInfoRepository;
        IReferalUserRepository refRepository;
        GoogleCredential credential;
        ITwitterService twitterService;
        IMemoryCache cache;

        public GoogleDriveService(
            IUserRepository userRepository,
            IUserInfoRepository userInfoRepository, 
            IReferalUserRepository refRepository, 
            ITwitterService twitterService,
            IMemoryCache cache)
        {
            this.userRepository = userRepository;
            this.refRepository = refRepository;
            this.twitterService = twitterService;
            this.userInfoRepository = userInfoRepository;
            this.cache = cache;

            using (var stream = new FileStream("RealEstateDocBountyBOT-1330d8174b49.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(
                    SheetsService.Scope.Spreadsheets, 
                    DriveService.Scope.DriveFile, 
                    DriveService.Scope.DriveMetadata, 
                    DriveService.Scope.Drive);
            }
        }

        /// <summary>
        /// <param name="columnName">columnName - Get from SaveSuccessMessageNotificationHandler</param>
        /// <returns></returns>
        public async Task<bool> ExportImageToGoogleDrive(int telegramUserId, string columnName, Stream streamFile)
        {
            var user = userRepository.GetUser(telegramUserId);

            var service = new DriveService(new BaseClientService.Initializer
            {
                ApplicationName = "RedBountyBot",
                HttpClientInitializer = credential
            });

            string fileName = "";

            if (user != null)
            {
                if (columnName.Contains("Facebook"))
                    columnName = "facebook";

                fileName = String.Format("{0}-{1}-{2}.jpg", user.Id, user.UserName, columnName);

                string queryGetFolder = "sharedWithMe and mimeType = 'application/vnd.google-apps.folder'";
                var folder = GetFirstFileOrFolder(queryGetFolder);

                if (folder != null)
                {
                    string queryGetFile = String.Format("'{0}' in parents and name contains '{1}' and name contains '{2}'",
                        folder.Id, user.UserName, columnName);
                    var file = GetFirstFileOrFolder(queryGetFile);

                    var image = new Google.Apis.Drive.v3.Data.File()
                    {
                        Name = fileName,
                        CreatedTime = DateTime.Now,
                        Parents = new List<string> { folder.Id },

                    };

                    service.Files.Create(image, streamFile, "image/jpeg").Upload();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task ExportUserInfo(int telegramUserId)
        {
            try
            {
                var user = userRepository.GetUser(telegramUserId);
                var userInfo = userInfoRepository.GetInfo(telegramUserId);
                if (user == null)
                    return;

                if (userInfo == null)
                {
                    userInfo = new Models.TelegramUserInfo() { TelegramUserId = user.Id };
                    userInfoRepository.CreateInfo(userInfo);
                }

                //referals
                var listReferals = refRepository.GetAllUserReferals(telegramUserId);
                int countReferals = 0;
                if (listReferals != null)
                    countReferals = listReferals.Count();

                var service = new SheetsService(new BaseClientService.Initializer
                {
                    ApplicationName = "RedBountyBot",
                    HttpClientInitializer = credential
                });

                string[][] newValues = new string[][]
                {
                new string[]{
                    DateTime.UtcNow.AddHours(8).ToString(),
                    user.Id.ToString(),
                    user.UserName ?? "username not defined",
                    (user.FirstName ?? "") + " " + (user.LastName ?? ""),
                    userInfo.Email ?? "",
                    userInfo.TwitterScreenName ?? "",
                    userInfo.TwitterIsFollowedBy == true ? "Yes" : "No",
                    userInfo.FacebookProfile ?? "",
                    countReferals.ToString(),
                    userInfo.TotalPoints.ToString(),
                    userInfo.EthereumAddress ?? "",
                    userInfo.Question1 ?? "",
                    userInfo.Question2 ?? "",
                    userInfo.Question3 ?? "",
                    userInfo.IsAllQuestionsAnswered ? "Yes" : "No",
                    user.IsRestricted ? "Yes" : "No"
                }
                };
                ValueRange body = new ValueRange();
                body.Range = range;
                body.Values = newValues;

                string currentSpreadsheetId = spreadSheetId1;

                var requestUsersIdColumn = service.Spreadsheets.Values.Get(currentSpreadsheetId, range + "!B1:B50000");
                var usersIdColumn = requestUsersIdColumn.Execute().Values;

                int positionUserIdInSheet = -1;
                if (usersIdColumn != null)
                {
                    positionUserIdInSheet = SearchPositionUserIdnSheets(user.Id, usersIdColumn);
                }

                // uncomment if need add info in 2nd spreadsheet
                //if (positionUserIdInSheet == -1)
                //{
                //    currentSpreadsheetId = spreadSheetId2;

                //    requestUsersIdColumn = service.Spreadsheets.Values.Get(currentSpreadsheetId, range + "!B1:B50000"); // 2. search in 2nd spreadsheet
                //    usersIdColumn = requestUsersIdColumn.Execute().Values;

                //    if (usersIdColumn != null)
                //    {
                //        positionUserIdInSheet = SearchPositionUserIdnSheets(user.Id, usersIdColumn);
                //    }
                //}

                if (positionUserIdInSheet != -1)
                {
                    var listCellData = new List<CellData>();

                    for (int i = 0; i < newValues[0].Length; i++)
                    {
                        listCellData.Add(new CellData()
                        {
                            UserEnteredValue = new ExtendedValue()
                            {
                                StringValue = newValues[0][i]
                            }
                        });
                    }

                    var listRequests = new List<Request>()
                {
                    new Request()
                    {
                        UpdateCells = new UpdateCellsRequest()
                        {
                            Rows = new List<RowData>()
                            {
                                new RowData(){ Values = listCellData  }
                            },
                            Fields = "*",
                            Start = new GridCoordinate()
                            {
                                 ColumnIndex = 0,
                                 RowIndex = positionUserIdInSheet,
                            }
                        }
                    }
                };

                    BatchUpdateSpreadsheetRequest r = new BatchUpdateSpreadsheetRequest()
                    {
                        Requests = listRequests
                    };

                    var update = service.Spreadsheets.BatchUpdate(r, currentSpreadsheetId);
                    var res = update.Execute();
                }
                else
                {
                    var append = service.Spreadsheets.Values.Append(body, currentSpreadsheetId, range);
                    append.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.RAW;
                    append.Execute();
                }
            }
            catch(Exception e)
            {

            }
        }
        
        private Google.Apis.Drive.v3.Data.File GetFirstFileOrFolder(string query)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                ApplicationName = "RedBountyBot",
                HttpClientInitializer = credential
            });

            var request = service.Files.List();
            request.Q = query;
            var files = request.Execute().Files;
            if (files != null && files.Count > 0)
                return files[0];
            else
                return null;
        }

        private int SearchPositionUserIdnSheets(int telegramUserId, IList<IList<object>> column)
        {
            int position = column.Select(x => x != null && x.Count > 0 ? x[0] : "-1").ToList().FindIndex(m => m != null && m.ToString() == telegramUserId.ToString());
            return position;
        }

        public async Task UpdateTwitterStatusesInSpreadsheets()
        {
            int twitterStatusPositionInRow = 6;

            var service = new SheetsService(new BaseClientService.Initializer
            {
                ApplicationName = "RedBountyBot",
                HttpClientInitializer = credential
            });

            List<Request> listRequests = new List<Request>();

            var requestUsersIdColumn = service.Spreadsheets.Values.Get(spreadSheetId1, range + "!B1:B50000");
            var usersIdColumn = requestUsersIdColumn.Execute().Values;

            var allUsersInfo = userInfoRepository.GetAllUsersInfo();
            foreach (var user in allUsersInfo)
            {
                int positionUserIdInSheet = -1;
                if (usersIdColumn != null)
                {
                    positionUserIdInSheet = SearchPositionUserIdnSheets(user.TelegramUserId, usersIdColumn);
                }

                if (positionUserIdInSheet != -1)
                {
                    var listCellData = new List<CellData>()
                {
                    new CellData()
                    {
                        UserEnteredValue = new ExtendedValue()
                        {
                            StringValue = user.TwitterIsFollowedBy ? "Yes" : "No"
                        }
                    }
                };

                    var rows = new List<RowData>();

                    rows.Add(new RowData()
                    {
                        Values = listCellData
                    });

                    listRequests.Add(new Request()
                    {
                        UpdateCells = new UpdateCellsRequest()
                        {
                            Rows = rows,
                            Fields = "*",
                            Start = new GridCoordinate()
                            {
                                ColumnIndex = twitterStatusPositionInRow,
                                RowIndex = positionUserIdInSheet,
                            }
                        }
                    });
                }
            }
            
            BatchUpdateSpreadsheetRequest r = new BatchUpdateSpreadsheetRequest()
            {
                Requests = listRequests
            };

            var update = service.Spreadsheets.BatchUpdate(r, spreadSheetId1);
            var res = update.Execute();

        }

        public async Task UpdateTotalPointsInSpreadsheets()
        {
            int totalPointsPositionInRow = 9;

            var service = new SheetsService(new BaseClientService.Initializer
            {
                ApplicationName = "RedBountyBot",
                HttpClientInitializer = credential
            });

            List<Request> listRequests = new List<Request>();

            var requestUsersIdColumn = service.Spreadsheets.Values.Get(spreadSheetId1, range + "!B1:B50000");
            var usersIdColumn = requestUsersIdColumn.Execute().Values;

            var allUsersInfo = userInfoRepository.GetAllUsersInfo();
            foreach (var user in allUsersInfo)
            {
                int positionUserIdInSheet = -1;
                if (usersIdColumn != null)
                {
                    positionUserIdInSheet = SearchPositionUserIdnSheets(user.TelegramUserId, usersIdColumn);
                }

                if (positionUserIdInSheet != -1)
                {
                    var listCellData = new List<CellData>()
                    {
                        new CellData()
                        {
                            UserEnteredValue = new ExtendedValue()
                            {
                                StringValue = user.TotalPoints.ToString()
                            }
                        }
                    };

                    var rows = new List<RowData>();

                    rows.Add(new RowData()
                    {
                        Values = listCellData
                    });

                    listRequests.Add(new Request()
                    {
                        UpdateCells = new UpdateCellsRequest()
                        {
                            Rows = rows,
                            Fields = "*",
                            Start = new GridCoordinate()
                            {
                                ColumnIndex = totalPointsPositionInRow,
                                RowIndex = positionUserIdInSheet,
                                SheetId = 1282710228
                            }
                        }
                    });
                }
            }

            BatchUpdateSpreadsheetRequest r = new BatchUpdateSpreadsheetRequest()
            {
                Requests = listRequests
            };

            var update = service.Spreadsheets.BatchUpdate(r, spreadSheetId1);
            var res = update.Execute();

        }

        public async Task<RandomQuestion> GetQuestion(int id)
        {
            try
            {
                var service = new SheetsService(new BaseClientService.Initializer
                {
                    ApplicationName = "RedBountyBotBountyBot",
                    HttpClientInitializer = credential
                });

                int rowStartIndexInSheets = 2;

                var requestSpreadsheet = service.Spreadsheets.Get(RandomQuestionsSpreadSheetId);
                requestSpreadsheet.IncludeGridData = true;
                requestSpreadsheet.Ranges = new List<string> { range + String.Format("!A{0}:G{0}", rowStartIndexInSheets + id) };


                var requestQuestionColumns = requestSpreadsheet.Execute();
                var values = requestQuestionColumns.Sheets[0].Data[0].RowData[0].Values;

                string title = values[0].FormattedValue.Trim();

                if (title == null)
                    return null;

                List<string> answers = new List<string>();

                for (int i = 1; i < values.Count; i++)
                {
                    if (values[i].FormattedValue != null)
                        answers.Add(values[i].FormattedValue.Trim());
                }

                var correctCell = values.First(x => 
                {
                    var backgrColor = x.EffectiveFormat.BackgroundColor;

                    return backgrColor.Blue != 1 &&
                        backgrColor.Green != 1 &&
                        backgrColor.Red != 1;
                });

                int correctIndex = answers.IndexOf(correctCell.FormattedValue.Trim());

                if(correctIndex != -1 && answers.Count > 1)  
                    return new RandomQuestion(title, answers, correctIndex);
                else
                    return null;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<int> GetCountQuestion()
        {
            try
            {
                var service = new SheetsService(new BaseClientService.Initializer
                {
                    ApplicationName = "RedBountyBotBountyBot",
                    HttpClientInitializer = credential
                });


                var requestQuestionColumns = service.Spreadsheets.Values.Get(RandomQuestionsSpreadSheetId, range + "!A2:A1000");
                var questionsColumns = requestQuestionColumns.Execute().Values;

                return questionsColumns.Count;
            }
            catch(Exception e)
            {
                return -1;
            }
        }
    }
}
