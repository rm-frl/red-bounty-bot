﻿using RedBountyBot.DAL;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RedBountyBot.Notifications
{
    public class SaveStateInDatabaseNotificationHandler : INotificationHandler<SaveStateNotification>
    {
        IUserRepository userRepository;

        public SaveStateInDatabaseNotificationHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public async Task Handle(SaveStateNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                var user = userRepository.GetUser(notification.TelegramUserId);
                if(user == null)
                {
                    var newUser = new TelegramUser()
                    {
                        Id = notification.TelegramUserId,
                        CurrentStateClassName = notification.CurrentStateClassName,
                        CurrentDialogClassName = notification.CurrentDialogClassName
                    
                    };
                    userRepository.CreateUser(newUser);
                }
                else
                {
                    user.CurrentDialogClassName = notification.CurrentDialogClassName;
                    user.CurrentStateClassName = notification.CurrentStateClassName;
                    userRepository.UpdateUser(user);
                }
            }
            catch(Exception e)
            {

            }
        }
    }
}
