﻿using RedBountyBot.Dialogs;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Models
{
    public class DialogFactory
    {
        public static readonly string START_COMMAND = "/start";
        public static readonly string RESET_COMMAND = "/reset";

        #region UserInfo props name
        public static readonly string Q1_FIELD = nameof(TelegramUserInfo.Question1);
        public static readonly string Q2_FIELD = nameof(TelegramUserInfo.Question2);
        public static readonly string Q3_FIELD = nameof(TelegramUserInfo.Question3);
        public static readonly string EMAIL_FIELD = nameof(TelegramUserInfo.Email);
        public static readonly string TWITTER_FIELD = nameof(TelegramUserInfo.TwitterScreenName);
        public static readonly string FACEBOOK_FIELD = nameof(TelegramUserInfo.FacebookProfile);
        public static readonly string ETHEREUM_FIELD = nameof(TelegramUserInfo.EthereumAddress);
        #endregion

        #region 0.1 Questions Dialog
        public static BaseDialog CreateRandomDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new QuestionRandomDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateQ1Dialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new Question1Dialog(mediator, currentState, telegramUserId, Q1_FIELD);
            return dialog;
        }

        public static BaseDialog CreateQ2Dialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new Question2Dialog(mediator, currentState, telegramUserId, Q2_FIELD);
            return dialog;
        }

        public static BaseDialog CreateQ3Dialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new Question3Dialog(mediator, currentState, telegramUserId, Q3_FIELD);
            return dialog;
        }
        #endregion

        #region 0.2 Main Menu Dialogs
        public static BaseDialog CreateMainMenuDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new MainMenuDialog(mediator, currentState, telegramUserId);
            return dialog;
        }
        #endregion
       
        #region 1. Create Profile Dialogs
        public static BaseDialog CreateProfileDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new CreateProfileDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateEmailDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new EmailDialog(mediator, currentState, telegramUserId, EMAIL_FIELD);
            return dialog;
        }

        public static BaseDialog CreateTwitterDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new TwitterDialog(mediator, currentState, telegramUserId, TWITTER_FIELD);
            return dialog;
        }

        public static BaseDialog CreateFacebookDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new FacebookDialog(mediator, currentState, telegramUserId, FACEBOOK_FIELD);
            return dialog;
        }

        public static BaseDialog CreateEthereumDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new EthereumDialog(mediator, currentState, telegramUserId, ETHEREUM_FIELD);
            return dialog;
        }
        #endregion

        #region 2. Show Details Dialogs
        public static BaseDialog CreateShowDetailsDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new ShowDetailsDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateMyProfileDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new MyProfileDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateGroupInvitersDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new GroupInvitersDialog(mediator, currentState, telegramUserId);
            return dialog;
        }

        public static BaseDialog CreateEarnedPointsDialog(IMediator mediator, string currentState, int telegramUserId)
        {
            var dialog = new EarnedPointsDialog(mediator, currentState, telegramUserId);
            return dialog;
        }
        #endregion
    }
}
