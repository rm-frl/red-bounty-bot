﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedBountyBot.DAL
{
    public interface IUserRepository
    {
        TelegramUser GetUser(int telegramUserId);
        Task<TelegramUser> GetUserAsync(int telegramUserId);
        void CreateUser(TelegramUser user);
        Task CreateUserAsync(TelegramUser user);
        void UpdateUser(TelegramUser user);
        Task UpdateUserAsync(TelegramUser user);
        void RemoveUser(int telegramUserId);
        Task RemoveUserAsync(int telegramUserId);

    }
}
