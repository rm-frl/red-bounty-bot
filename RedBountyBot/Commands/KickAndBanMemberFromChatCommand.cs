﻿using RedBountyBot.Interfaces;
using MediatR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class KickAndBanMemberFromChatCommand: IRequest
    {
        public int TelegramUserId { get; private set; }

        public KickAndBanMemberFromChatCommand(int telegramUserId) => TelegramUserId = telegramUserId;
    }

    public class KickAndBanMemberFromChatCommandHandler : AsyncRequestHandler<KickAndBanMemberFromChatCommand>
    {
        IClientService telegramService;
        IOptions<TelegramConfiguration> config;

        public KickAndBanMemberFromChatCommandHandler(IOptions<TelegramConfiguration> config, IClientService telegramService)
        {
            this.telegramService = telegramService;
            this.config = config;
        }
        protected override async Task HandleCore(KickAndBanMemberFromChatCommand request)
        {
            try
            { 
                await telegramService.Client.RestrictChatMemberAsync(config.Value.TargetGroupName, request.TelegramUserId);
                await telegramService.Client.KickChatMemberAsync(config.Value.TargetGroupName, request.TelegramUserId);
            }
            catch(Exception e)
            {

            }
        }
    }
}
