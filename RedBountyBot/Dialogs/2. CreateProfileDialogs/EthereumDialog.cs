﻿using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using RedBountyBot.Queries;

namespace RedBountyBot.Dialogs
{
    public class EthereumDialog : BaseDialog
    {
        public EthereumDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage =>
            "Please key in your Ethereum address. Please ensure that this is an Ethereum address which you have absolute control over.\n\n" +
            "NOTE: DO NOT KEY IN ANY EXCHANGE ADDRESS. Real Estate Doc is not responsible for any loss of bounty tokens due to the inputting of exchange address.";

        public override string ErrorTextMessage =>
            "Sorry! That was not a Ethereum address format. Please enter a valid Ethereum address.\n\n" +
            "Again, please ensure that this is an Ethereum address which you have absolute control over.\n\n" +
            "NOTE: DO NOT ENTER ANY EXCHANGE ADDRESS. Real Estate Doc is not responsible for any loss of bounty tokens due to the inputting of exchange address. 😎👍";

        public override string SuccessTextMessage =>
            "Thank you! We have recorded your Ethereum address!\n" +
            "NOTE: DO NOT ENTER IN ANY EXCHANGE ADDRESS. Real Estate Doc is not responsible for any loss of bounty tokens due to the inputting of exchange address.";

        public override bool IsValidMessage(Update update)
        {
            try
            {
                if (update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage
                    && update.Message.Text.StartsWith("0x") && update.Message.Text.Length == 42)
                {
                    string column = nameof(TelegramUserInfo.EthereumAddress);
                    string value = update.Message.Text;
                    var checkDuplicatesInDatabaseQuery = new CheckDuplicatesInDatabaseQuery(this.TelegramUserId, column, value);
                    var containsAny = Mediator.Send(checkDuplicatesInDatabaseQuery).Result;
                    if (!containsAny)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

    }
}
