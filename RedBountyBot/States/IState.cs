﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public interface IState
    {
        void HandleMessage(Update update);
        ReplyTextMessage GetReply();
        void OnEntry();
        void OnExit();
    }
}
