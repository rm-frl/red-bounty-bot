﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RedBountyBot.Models
{
    public class ReferalUser
    {
        public int Id { get; set; }
        public int FromId { get; set; }
        public int ReferalUserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [ForeignKey("FromId")]
        public virtual TelegramUser From { get; set; }
    }
}
