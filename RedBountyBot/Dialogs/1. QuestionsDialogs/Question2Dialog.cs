﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RedBountyBot.Commands;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Dialogs
{
    public class Question2Dialog : BaseQuestionDialog
    {
        public Question2Dialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, currentState, telegramUserId)
        {
            Name = name;
        }

        private readonly string Question = "Where did you hear this Bounty and Referral program from?";
        private readonly string Error = "You have entered the wrong type of answer, please try again";

        public override ReplyTextMessage InitMessage => GetInitMessage();
        public override ReplyTextMessage ErrorMessage => GetErrorMessageMessage();
        public override int NumberAttempts => 10;

        public override void CallNextDialog()
        {
            DialogFactory.CreateQ3Dialog(Mediator, null, TelegramUserId);
        }


        private ReplyTextMessage GetInitMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Question } },
            };
            return reply;
        }

        private ReplyTextMessage GetErrorMessageMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Error } },
            };
            return reply;
        }

    }
}
