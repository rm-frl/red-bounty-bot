﻿using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Dialogs
{
    public class MyProfileDialog: BaseDialog
    {
        public const string EDIT_PROFILE_LABEL = "✏️ Edit My Profile";

        public MyProfileDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, telegramUserId)
        {
            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new ShowMenuMyProfileState(this));
            }
        }
    }
}
