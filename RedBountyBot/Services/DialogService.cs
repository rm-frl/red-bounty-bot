﻿using RedBountyBot.DAL;
using RedBountyBot.Dialogs;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.Services
{
    public class DialogService : IDialogService
    {
        IMediator mediator;
        IUserRepository userRepository;

        public DialogService(IMediator mediator, IUserRepository userRepository)
        {
            this.userRepository = userRepository;
            this.mediator = mediator;

            dialogDictionary = new Dictionary<string, string>();
            dialogDictionary.Add("/start", nameof(DialogFactory.CreateMainMenuDialog));
            dialogDictionary.Add(typeof(QuestionRandomDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateRandomDialog));
            dialogDictionary.Add(typeof(Question1Dialog).AssemblyQualifiedName, nameof(DialogFactory.CreateQ1Dialog));
            dialogDictionary.Add(typeof(Question2Dialog).AssemblyQualifiedName, nameof(DialogFactory.CreateQ2Dialog));
            dialogDictionary.Add(typeof(Question3Dialog).AssemblyQualifiedName, nameof(DialogFactory.CreateQ3Dialog));
            dialogDictionary.Add(typeof(MainMenuDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateMainMenuDialog));
            dialogDictionary.Add(typeof(CreateProfileDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateProfileDialog));
            dialogDictionary.Add(typeof(ShowDetailsDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateShowDetailsDialog));
            dialogDictionary.Add(typeof(MyProfileDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateMyProfileDialog));
            dialogDictionary.Add(typeof(GroupInvitersDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateGroupInvitersDialog));
            dialogDictionary.Add(typeof(EarnedPointsDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateEarnedPointsDialog));
            dialogDictionary.Add(typeof(EmailDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateEmailDialog));
            dialogDictionary.Add(typeof(TwitterDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateTwitterDialog));
            dialogDictionary.Add(typeof(FacebookDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateFacebookDialog));
            dialogDictionary.Add(typeof(EthereumDialog).AssemblyQualifiedName, nameof(DialogFactory.CreateEthereumDialog));
        }

        Dictionary<string, string> dialogDictionary;

        public bool SendReplyToUser(Update update, TelegramUser user)
        {
            if (user == null)
                return false;

            var dialogClass = user.CurrentDialogClassName ?? "/start" ;
            var stateClass = user.CurrentStateClassName;

            BaseDialog dialog = null;
            if (dialogDictionary.ContainsKey(dialogClass))
            {
                string methodName = dialogDictionary[dialogClass];
                dialog = typeof(DialogFactory).GetMethod(methodName).Invoke(null, new object[] { mediator, stateClass, user.Id }) as BaseDialog;
            }

            if(dialog != null)
            {
                dialog.HandleMessage(update);
            }
            return false;
        }

        public Task<bool> SendReplyToUserAsync(Update update)
        {
            throw new NotImplementedException();
        }
    }
}
