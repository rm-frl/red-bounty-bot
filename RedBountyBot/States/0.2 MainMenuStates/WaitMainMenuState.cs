﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class WaitMainMenuState: BaseState
    {
        public WaitMainMenuState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void HandleMessage(Update update)
        {
            switch (update.Message.Text)
            {
                case MainMenuDialog.CREATE_PROFILE_LABEL:
                    DialogFactory.CreateProfileDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case MainMenuDialog.SHOW_DETAILS_LABEL:
                    DialogFactory.CreateShowDetailsDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
            }
        }
    }
}
