﻿using RedBountyBot.Dialogs;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class ShowMenuMyProfileState: BaseState
    {
        TelegramUserInfo UserInfo { get; set; }
        bool IsFollowUser { get; }

        public ShowMenuMyProfileState(BaseDialog Dialog) : base(Dialog)
        {
            GetAllUserInfoQuery query = new GetAllUserInfoQuery(Dialog.TelegramUserId);
            UserInfo = Dialog.Mediator.Send(query).Result;
            if (UserInfo?.TwitterScreenName != null)
                IsFollowUser = Dialog.Mediator.Send(new CheckFollowersInTwitterQuery(UserInfo.TwitterScreenName)).Result;
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitMenuMyProfileState(Dialog));
        }

        public ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                 new KeyboardButton[]{
                    new KeyboardButton(MyProfileDialog.EDIT_PROFILE_LABEL),
                },
                  new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = CreateProfileInfo() } },
                ParseMode = Telegram.Bot.Types.Enums.ParseMode.Html,
                ReplyMarkup = keyboard
            };
            return reply;
        }

        private string CreateProfileInfo()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append("<b>YOUR PROFILE</b>");
            strBuilder.Append("\n");
            strBuilder.AppendFormat("{0} Email : {1}\n", 
                (UserInfo?.Email != null ? "✔️" : "❌"),
                (UserInfo?.Email != null ? UserInfo.Email : "Not filled")
                );
            strBuilder.AppendFormat("{0} Twitter Handle : {1}\n",
                (UserInfo?.TwitterScreenName != null ? "✔️" : "❌"),
                (UserInfo?.TwitterScreenName != null ? UserInfo.TwitterScreenName : "Not filled")
                );
            strBuilder.AppendFormat("{0} Facebook Photo : {1}\n",
                (UserInfo?.FacebookProfile != null ? "✔️" : "❌"),
                (UserInfo?.FacebookProfile != null ? "Attached" : "Not Attached")
                );
            strBuilder.AppendFormat("{0} Ethereum Address : {1}\n",
                (UserInfo?.EthereumAddress != null ? "✔️" : "❌"),
                (UserInfo?.EthereumAddress != null ? UserInfo.EthereumAddress : "Not filled")
                );
            strBuilder.AppendFormat("{0} Following Real Estate Doc on Twitter : {1}\n\n",
                (IsFollowUser == true ? "✔️" : "❌"),
                (IsFollowUser == true ? "Yes" : "No")
                );
            strBuilder.Append("😁👍");
            return strBuilder.ToString();
        }
    }
}
