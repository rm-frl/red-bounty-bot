﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class CalculatePointsCommand: IRequest<CalculatedPointsDTO>
    {
        public int TelegramUserId { get; private set; }
        public TelegramUserInfo UserInfo { get; private set; }

        public CalculatePointsCommand(int telegramUserId, TelegramUserInfo userInfo = null)
        {
            TelegramUserId = telegramUserId;
            if (userInfo != null) UserInfo = userInfo;
        }
    }

    public class CalculatePointsCommandHandler : AsyncRequestHandler<CalculatePointsCommand, CalculatedPointsDTO>
    {
        const int POINTS_IN_GROUP = 200;
        const int POINTS_FOLLOW_TWITTER = 150;
        const int POINTS_FACEBOOK_SCREENSHOT = 150;
        const int POINTS_MEMBER_MULTIPLICATOR = 200;
        const int REFERALS_MAX_COUNT = 10;

        IMediator mediator;
        IUserInfoRepository userInfoRepository;
        IGoogleDriveService googleDriveService;

        public CalculatePointsCommandHandler(IMediator mediator, IUserInfoRepository userInfoRepository, IGoogleDriveService googleDriveService)
        {
            this.mediator = mediator;
            this.userInfoRepository = userInfoRepository;
            this.googleDriveService = googleDriveService;
        }

        protected override async Task<CalculatedPointsDTO> HandleCore(CalculatePointsCommand request)
        {
            int telegramUserId = request.TelegramUserId;

            try
            {
                bool inGroup = false ;
                int referalsCount = 0;
                TelegramUserInfo userInfo = new TelegramUserInfo();
                
                var checkMemberInGroupQuery = new CheckMemberInGroupQuery(telegramUserId);
                inGroup = await mediator.Send(checkMemberInGroupQuery) != null ? true : false;
               
                var getReferalsCountQuery = new GetReferalsCountQuery(telegramUserId);
                referalsCount = await mediator.Send(getReferalsCountQuery);

                if (request.UserInfo != null)
                    userInfo = request.UserInfo;
                else
                    userInfo = userInfoRepository.GetInfo(telegramUserId);

                CalculatedPointsDTO pointsDTO = new CalculatedPointsDTO()
                {
                    InGroupPoints = inGroup ? POINTS_IN_GROUP : 0,
                    TwitterPoints = userInfo.TwitterIsFollowedBy ? POINTS_FOLLOW_TWITTER : 0,
                    FacebookPoints = userInfo.FacebookProfile != null ? POINTS_FACEBOOK_SCREENSHOT : 0,
                    ReferalsPoints = (referalsCount <= REFERALS_MAX_COUNT ? referalsCount : REFERALS_MAX_COUNT) * POINTS_MEMBER_MULTIPLICATOR,
                    ReferalsCount = referalsCount
                };

                if (userInfo.TotalPoints != pointsDTO.TotalPoints)
                {
                    userInfo.TotalPoints = pointsDTO.TotalPoints;
                    userInfoRepository.UpdateInfo(userInfo);
                }

                return pointsDTO;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }

}
