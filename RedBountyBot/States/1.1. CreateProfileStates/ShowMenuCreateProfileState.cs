﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class ShowMenuCreateProfileState: BaseState
    {
        public ShowMenuCreateProfileState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitCreateProfileState(Dialog));
        }

        public override ReplyTextMessage GetReply()
        {
            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = false;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                new KeyboardButton[]{
                    new KeyboardButton(CreateProfileDialog.EMAIL_LABEL),
                    new KeyboardButton(CreateProfileDialog.TWITTER_LABEL),
                },
                new KeyboardButton[]{
                    new KeyboardButton(CreateProfileDialog.FACEBOOK_LABEL),
                    new KeyboardButton(CreateProfileDialog.ETHEREUM_LABEL),
                },
                new KeyboardButton[]{
                    new KeyboardButton(CreateProfileDialog.RETURN_BACK_LABEL),
                },
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = "Create Profile!\n\nMake sure you follow the social media platforms before submitting your details to the bot!" } },
                ReplyMarkup = keyboard
            };
            return reply;
        }
    }
}
