﻿using RedBountyBot.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedBountyBot.DAL
{
    public class DatabaseContext: DbContext
    {
        public DbSet<TelegramUser> Users { get; set; }

        public DbSet<TelegramUserInfo> UsersInfo { get; set; }

        public DbSet<ReferalUser> ReferalUsers { get; set; }
        
        public DatabaseContext(DbContextOptions<DatabaseContext> options): base(options)
        {
        }

    }
}
