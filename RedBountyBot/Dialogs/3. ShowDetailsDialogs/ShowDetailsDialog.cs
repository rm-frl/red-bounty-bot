﻿using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Dialogs
{
    public class ShowDetailsDialog: BaseDialog
    {
        public const string SHOW_MY_PROFILE_LABEL = "👓 Show My Real Estate Doc Bounty Profile";
        public const string SHOW_GROUP_INVITERS = "🤝 Members Invites";
        public const string SHOW_EARNED_POINTS = "💰 Earned Points";

        public ShowDetailsDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, telegramUserId)
        {
            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new ShowMenuShowDetailsState(this));
            }
        }
    }
}
