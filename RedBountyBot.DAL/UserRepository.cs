﻿using RedBountyBot.Models;
using System;
using System.Threading.Tasks;

namespace RedBountyBot.DAL
{
    public class UserRepository : IUserRepository
    {
        DatabaseContext context;

        public UserRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public TelegramUser GetUser(int telegramUserId)
        {
            return context.Users.Find(telegramUserId);
        }

        public void CreateUser(TelegramUser user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void RemoveUser(int telegramUserId)
        {
            context.Users.Remove(new TelegramUser() { Id = telegramUserId });
        }

        public void UpdateUser(TelegramUser user)
        {
            context.Users.Update(user);
            context.SaveChanges();
        }

        public async Task<TelegramUser> GetUserAsync(int telegramUserId)
        {
            return await context.Users.FindAsync(telegramUserId);
        }

        public async Task CreateUserAsync(TelegramUser user)
        {
            context.Users.Add(user);
            await context.SaveChangesAsync();
        }

        public async Task RemoveUserAsync(int telegramUserId)
        {
            context.Users.Remove(new TelegramUser() { Id = telegramUserId });
            await context.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(TelegramUser user)
        {
            context.Users.Update(user);
            await context.SaveChangesAsync();
        }
    }
}
