﻿using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;
namespace RedBountyBot.Notifications
{
    public class SaveSuccessMessageNotificationHandler : INotificationHandler<SaveSuccessMessageNotification>
    {
        IUserInfoRepository userInfoRepository;
        IClientService clientService;
        IGoogleDriveService googleService;

        public SaveSuccessMessageNotificationHandler(IUserInfoRepository userInfoRepository, 
            IClientService clientService, IGoogleDriveService googleService)
        {
            this.userInfoRepository = userInfoRepository;
            this.clientService = clientService;
            this.googleService = googleService;
        }

        public async Task Handle(SaveSuccessMessageNotification notification, CancellationToken cancellationToken)
        {
            int userId = notification.Update.Message.From.Id;
            string columnName = notification.ColumnName;
            var info = userInfoRepository.GetInfo(userId);
            if (info == null)
            {
                info = new TelegramUserInfo()
                {
                    TelegramUserId = userId,
                };
                userInfoRepository.CreateInfo(info);
            }
            var prop = info.GetType().GetProperty(columnName);

            if (notification.Update.Message.Type == MessageType.TextMessage)
            {
                prop.SetValue(info, notification.Update.Message.Text);
                userInfoRepository.UpdateInfo(info);
            }
            else if(notification.Update.Message.Type == MessageType.PhotoMessage)
            {
                var photoArray = notification.Update.Message.Photo;
                string fileId = photoArray[photoArray.Length - 1].FileId;
                Telegram.Bot.Types.File file = clientService.Client.GetFileAsync(fileId).Result;

                var uploadResult = googleService.ExportImageToGoogleDrive(userId, columnName, file.FileStream).Result;

                if (uploadResult)
                {
                    prop.SetValue(info, "stored in google drive");
                    userInfoRepository.UpdateInfo(info);
                }
            }

            //googleService.ExportUserInfo(userId);
        }
    }
}
