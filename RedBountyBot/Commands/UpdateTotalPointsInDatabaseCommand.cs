﻿using RedBountyBot.DAL;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateTotalPointsInDatabaseCommand: IRequest
    {
    }

    public class UpdateTotalPointsInDatabaseCommandHandler : AsyncRequestHandler<UpdateTotalPointsInDatabaseCommand>
    {
        IMediator mediator;
        IUserInfoRepository userInfoRepository;

        public UpdateTotalPointsInDatabaseCommandHandler(IMediator mediator, IUserInfoRepository userInfoRepository)
        {
            this.mediator = mediator;
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task HandleCore(UpdateTotalPointsInDatabaseCommand request)
        {
            var allUsersInfo = userInfoRepository.GetAllUsersInfo();

            int i = 0;
            foreach (var user in allUsersInfo)
            {
                i++;
                var calcPointsCommand = new CalculatePointsCommand(user.TelegramUserId, user);
                mediator.Send(calcPointsCommand);
            }

        }
    }
}
