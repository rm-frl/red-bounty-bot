﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class ErrorValueState: BaseState
    {
        public ErrorValueState(BaseDialog Dialog) : base(Dialog) { }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitValueState(Dialog));
        }

        public override ReplyTextMessage GetReply()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Dialog.ErrorTextMessage } }
            };

            return reply;
        }
    }
}
