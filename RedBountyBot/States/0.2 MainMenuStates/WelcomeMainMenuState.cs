﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class WelcomeMainMenuState: BaseState
    {
        public WelcomeMainMenuState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new ShowMainMenuState(Dialog));
        }

        public ReplyTextMessage GetReply()
        {
            string text = @"Thank you for joining the Telegram Group! Press/click on ‘Create my REAL ESTATE DOC Bounty Profile’ to key in your
        🔹Email address,
        🔹Ethereum address,
        🔹Twitter handle,
        and to submit the following:
        🔹Facebook screenshot showing you have followed REAL ESTATE DOC's Facebook page

        🔔 A total of 2500 REDT tokens could be yours!

        ▪️ 200 REDT tokens upon submission and verification of the information you've provided! Make sure you stay on our telegram group! 

        ▪️ 200 REDT tokens for each member you’ve successfully referred, capped at a maximum of 10 members each!

        ▪️ 150 REDT tokens following our Twitter and share the pinned tweet (https://twitter.com/doc_estate)!

        ▪️ 150 REDT tokens for liking our Facebook page (https://www.facebook.com/realestatedoc.io)!

        * Make sure that you are following REAL ESTATE DOC on Twitter and you’ve taken a Facebook screenshot before interacting with this bot if you wish to earn the bounty!";

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                ParseMode = ParseMode.Html
            };
            return reply;
        }
    }
}
