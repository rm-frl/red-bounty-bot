﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.Interfaces
{
    public interface IDialogService
    {
        bool SendReplyToUser(Update update, TelegramUser user);
        Task<bool> SendReplyToUserAsync(Update update);
    }
}
