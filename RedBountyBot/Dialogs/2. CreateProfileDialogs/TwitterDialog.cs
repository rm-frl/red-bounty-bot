﻿using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using RedBountyBot.Commands;
using RedBountyBot.Queries;

namespace RedBountyBot.Dialogs
{
    public class TwitterDialog : BaseDialog
    {
        public TwitterDialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, telegramUserId)
        {
            Name = name;
            if(currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitValueState(this)); //ChangeState() invoke OnEntry
            }
        }

        public override string InitTextMessage => "Please enter your Twitter handle (the handle is your personal ID on twitter, it starts with the ‘@’ symbol).";
        public override string ErrorTextMessage => "Sorry! That was an invalid Twitter handle. Please enter the correct handle.";
        public override string SuccessTextMessage => "Thank you! We have recorded your Twitter handle!";

        public override bool IsValidMessage(Update update)
        {
            if(update.Message.Type == Telegram.Bot.Types.Enums.MessageType.TextMessage && update.Message.Text.StartsWith("@"))
            {
                string column = nameof(TelegramUserInfo.TwitterScreenName);
                string value = update.Message.Text;
                var checkDuplicatesInDatabaseQuery = new CheckDuplicatesInDatabaseQuery(this.TelegramUserId, column, value);
                var containsAny = Mediator.Send(checkDuplicatesInDatabaseQuery).Result;
                if (!containsAny)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public override void HandleMessage(Update update)
        {
            if(update.Message.Text == RETURN_BACK_LABEL)
            {
                DialogFactory.CreateProfileDialog(Mediator, null, TelegramUserId);
                return;
            }
            base.HandleMessage(update);
        }

        public override void SaveSuccessMessage(Update successMessage)
        {
            var saveTwitterInfoCommand = new SaveTwitterInfoCommand(successMessage.Message.Text, TelegramUserId);
            Mediator.Send(saveTwitterInfoCommand).Wait();
            base.SaveSuccessMessage(successMessage);
        }
    }
}
