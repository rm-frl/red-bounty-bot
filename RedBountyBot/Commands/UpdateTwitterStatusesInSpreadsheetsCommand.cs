﻿using MediatR;
using RedBountyBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateTwitterStatusesInSpreadsheetsCommand: IRequest
    {
    }

    public class UpdateTwitterStatusesInSpreadsheetsCommandHandler : AsyncRequestHandler<UpdateTwitterStatusesInSpreadsheetsCommand>
    {
        IGoogleDriveService googleDriveService;

        public UpdateTwitterStatusesInSpreadsheetsCommandHandler(IGoogleDriveService googleDriveService)
        {
            this.googleDriveService = googleDriveService;
        }

        protected override async Task HandleCore(UpdateTwitterStatusesInSpreadsheetsCommand request)
        {
            await googleDriveService.UpdateTwitterStatusesInSpreadsheets();
        }
    }

}
