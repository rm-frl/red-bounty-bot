﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class WaitMenuMyProfileState: BaseState
    {
        public WaitMenuMyProfileState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void HandleMessage(Update update)
        {
            switch (update.Message.Text)
            {
                case MyProfileDialog.EDIT_PROFILE_LABEL:
                    DialogFactory.CreateProfileDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case BaseDialog.RETURN_BACK_LABEL:
                    DialogFactory.CreateShowDetailsDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
            }
        }
    }
}
