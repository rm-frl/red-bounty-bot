﻿using RedBountyBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateTotalPointsInSpreadsheetsCommand: IRequest
    {
    }

    public class UpdateTotalPointsInSpreadsheetsCommandHandler : AsyncRequestHandler<UpdateTotalPointsInSpreadsheetsCommand>
    {
        IGoogleDriveService googleDriveService;

        public UpdateTotalPointsInSpreadsheetsCommandHandler(IGoogleDriveService googleDriveService) => this.googleDriveService = googleDriveService;

        protected override async Task HandleCore(UpdateTotalPointsInSpreadsheetsCommand request)
        {
            googleDriveService.UpdateTotalPointsInSpreadsheets();
        }
    }
}
