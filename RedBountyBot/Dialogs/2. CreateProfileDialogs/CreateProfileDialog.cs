﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RedBountyBot.Models;
using RedBountyBot.States;
using MediatR;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Dialogs
{
    public class CreateProfileDialog: BaseDialog
    {
        public const string EMAIL_LABEL = "⚪ Enter Your Email Address";
        public const string TWITTER_LABEL = "⚪ Enter Your Twitter Handle";
        public const string FACEBOOK_LABEL = "⚪ Submit Your Facebook Proof-of-Following";
        public const string ETHEREUM_LABEL = "⚪ Enter Your Ethereum Address";

        public CreateProfileDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, telegramUserId)
        {
            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new ShowMenuCreateProfileState(this));
            }
        }
    }
}
