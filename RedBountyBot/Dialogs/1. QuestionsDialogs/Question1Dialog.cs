﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RedBountyBot.Commands;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Dialogs
{
    public class Question1Dialog : BaseQuestionDialog
    {
        public Question1Dialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, currentState, telegramUserId)
        {
            Name = name;
        }

        private readonly string Question = "Which country are you from?";
        private readonly string Error = "You have entered the wrong type of answer, please try again";

        public override ReplyTextMessage InitMessage => GetInitMessage();
        public override ReplyTextMessage ErrorMessage => GetErrorMessageMessage();
        public override int NumberAttempts => 10;

        public override void CallNextDialog()
        {
            DialogFactory.CreateQ2Dialog(Mediator, null, TelegramUserId);
        }

        private ReplyTextMessage GetInitMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Question } },
            };
            return reply;
        }

        private ReplyTextMessage GetErrorMessageMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Error } },
            };
            return reply;
        }

    }
}
