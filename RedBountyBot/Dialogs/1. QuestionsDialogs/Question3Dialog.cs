﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RedBountyBot.Commands;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Dialogs
{
    public class Question3Dialog : BaseQuestionDialog
    {
        public Question3Dialog(IMediator mediator, string currentState, int telegramUserId, string name) : base(mediator, currentState, telegramUserId)
        {
            Name = name;
        }

        private readonly string Question = "What kind of activities do you suggest Real Estate Doc to do for this community?";
        private readonly string Error = "You have entered the wrong type of answer, please try again";

        public override ReplyTextMessage InitMessage => GetInitMessage();
        public override ReplyTextMessage ErrorMessage => GetErrorMessageMessage();
        public override int NumberAttempts => 10;

        public override void CallNextDialog()
        {
            var updateQuestionInfoCommand = new UpdateQuestionInfoCommand(TelegramUserId)
            {
                IsAllQuestionsAnswered = true
            };
            Mediator.Send(updateQuestionInfoCommand);

            DialogFactory.CreateMainMenuDialog(Mediator, null, TelegramUserId);
        }

        private ReplyTextMessage GetInitMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Question } },
            };
            return reply;
        }

        private ReplyTextMessage GetErrorMessageMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = Error } },
            };
            return reply;
        }

    }
}
