﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using RedBountyBot.Services;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

namespace RedBountyBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(options => 
            options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection"), 
                b=> b.MigrationsAssembly("RedBountyBot.DAL")));

            services.AddMediatR();

            services.AddMemoryCache();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserInfoRepository, UserInfoRepository>();
            services.AddTransient<IReferalUserRepository, ReferalUserRepository>();

            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<ITwitterService, TwitterService>();
            services.AddScoped<IGoogleDriveService, GoogleDriveService>();
            services.AddScoped<IDialogService, DialogService>();

            services.Configure<TelegramConfiguration>(Configuration.GetSection("TelegramConfiguration"));
            services.Configure<TwitterConfiguration>(Configuration.GetSection("Twitter"));

            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
