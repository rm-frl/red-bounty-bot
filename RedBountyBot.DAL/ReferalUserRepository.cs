﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RedBountyBot.Models;

namespace RedBountyBot.DAL
{
    public class ReferalUserRepository : IReferalUserRepository
    {
        DatabaseContext context;

        public ReferalUserRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public bool AddReferal(ReferalUser referal)
        {
            var referalInDb = context.ReferalUsers.FirstOrDefault(x => x.ReferalUserId == referal.ReferalUserId);
            if (referalInDb == null)
            {
                context.ReferalUsers.Add(referal);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<ReferalUser> GetAllUserReferals(int telegramUserId)
        {
            return context.ReferalUsers.Where(x => x.FromId == telegramUserId).ToList();
        }

        public int? GetFromId(int referalUserId)
        {
            var referal = context.ReferalUsers.Where(x => x.ReferalUserId == referalUserId).FirstOrDefault();
            return referal?.FromId;
        }

        public bool RemoveReferal(int referalUserId)
        {
            var referalUser = context.ReferalUsers.FirstOrDefault(x => x.ReferalUserId == referalUserId);
            if (referalUser != null)
            {
                context.ReferalUsers.Remove(referalUser);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
