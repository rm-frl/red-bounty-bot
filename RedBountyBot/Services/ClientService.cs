﻿using RedBountyBot.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;

namespace RedBountyBot.Services
{
    public class ClientService: IClientService
    {
        public TelegramConfiguration Config { get; }

        public ClientService(IOptions<TelegramConfiguration> config)
        {
            Config = config.Value;
            Client = new TelegramBotClient(Config.BotToken);
        }

        public TelegramBotClient Client { get; }
    }
}
