﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.Notifications
{
    public class SaveSuccessMessageNotification: INotification
    {
        public Update Update { get; private set; }
        public string ColumnName { get; private set; }

        public SaveSuccessMessageNotification(string columnName, Update successMessage)
        {
            Update = successMessage;
            ColumnName = columnName;
        }
    }
}
