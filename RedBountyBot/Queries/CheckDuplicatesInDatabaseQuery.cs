﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class CheckDuplicatesInDatabaseQuery: IRequest<bool>
    {
        public int TelegramUserId { get; private set; }
        public string Column { get; private set; }
        public string Value { get; private set; }

        public CheckDuplicatesInDatabaseQuery(int telegramUserId, string column, string value)
        {
            TelegramUserId = telegramUserId;
            Column = column;
            Value = value;
        }
    }

    public class CheckDuplicatesInDatabaseQueryHandler : AsyncRequestHandler<CheckDuplicatesInDatabaseQuery, bool>
    {
        IUserInfoRepository userInfoRepository;

        public CheckDuplicatesInDatabaseQueryHandler(IUserInfoRepository userInfoRepository)
        {
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task<bool> HandleCore(CheckDuplicatesInDatabaseQuery request)
        {
            return userInfoRepository.ContainsAny(request.TelegramUserId, request.Column, request.Value);
        }
    }
}
