﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetReferalsCountQuery: IRequest<int>
    {
        public int TelegramUserId { get; set; }

        public GetReferalsCountQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }

    public class GetReferalsCountQueryHandler : AsyncRequestHandler<GetReferalsCountQuery, int>
    {
        IReferalUserRepository refUserRepository;

        public GetReferalsCountQueryHandler(IReferalUserRepository userRepository)
        {
            this.refUserRepository = userRepository;
        }

        protected override async Task<int> HandleCore(GetReferalsCountQuery request)
        {
            return refUserRepository.GetAllUserReferals(request.TelegramUserId).Count();
        }
    }

}
