﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Models
{
    public class ReplyTextMessage
    {
        public Update Update { get; set; }

        public ParseMode ParseMode { get; set; } = ParseMode.Markdown;

        public bool DisableWebPageView { get; set; } = true;

        public IReplyMarkup ReplyMarkup { get; set; }
    }

}
