﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Models
{
    public class RandomQuestion
    {
        public string Title { get; private set; }
        public List<string> Answers { get; private set; }
        public int RightAnswerIndex { get; private set; }

        public RandomQuestion(string title, List<string> answers, int rightAnswerIndex)
        {
            Title = title;
            Answers = answers;
            RightAnswerIndex = rightAnswerIndex;
        }
    }
}
