﻿using MediatR;
using RedBountyBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateUserInfoInGoogleSpreadsheetsCommand: IRequest
    {
        public int TelegramUserId { get; private set; }

        public UpdateUserInfoInGoogleSpreadsheetsCommand(int telegramUserId) => TelegramUserId = telegramUserId;
    }

    public class UpdateUserInfoInGoogleSpreadsheetsCommandHandler : AsyncRequestHandler<UpdateUserInfoInGoogleSpreadsheetsCommand>
    {
        IGoogleDriveService googleDriveService;

        public UpdateUserInfoInGoogleSpreadsheetsCommandHandler(IGoogleDriveService googleDriveService) => this.googleDriveService = googleDriveService;

        protected override async Task HandleCore(UpdateUserInfoInGoogleSpreadsheetsCommand request)
        {
            await googleDriveService.ExportUserInfo(request.TelegramUserId);
        }
    }

}
