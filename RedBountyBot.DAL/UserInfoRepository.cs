﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedBountyBot.Models;
using Microsoft.EntityFrameworkCore;

namespace RedBountyBot.DAL
{
    public class UserInfoRepository : IUserInfoRepository
    {
        DatabaseContext context;

        public UserInfoRepository(DatabaseContext context)
        {
            this.context = context;
        }

        public void CreateInfo(TelegramUserInfo info)
        {
            context.UsersInfo.Add(info);
            context.SaveChanges();
        }

        public List<TelegramUserInfo> GetAllUsersInfo()
        {
            return context.UsersInfo.ToList();
        }

        public TelegramUserInfo GetInfo(int telegramUserId)
        {
            var userInfo = context.UsersInfo.FirstOrDefault(x=>x.TelegramUserId == telegramUserId);
            return userInfo;
        }

        public async Task<TelegramUserInfo> GetInfoAsync(int telegramUserId)
        {
            var userInfo = await context.UsersInfo.FirstOrDefaultAsync(x => x.TelegramUserId == telegramUserId);
            return userInfo;
        }

        public void RemoveInfo(int telegramUserId)
        {
            var userInfo = context.Users.Find(telegramUserId).Info;
            context.UsersInfo.Remove(userInfo);
            context.SaveChanges();
        }

        public void UpdateInfo(TelegramUserInfo info)
        {
            context.UsersInfo.Update(info);
            context.SaveChanges();
        }

        public async Task UpdateInfoAsync(TelegramUserInfo info)
        {
            context.UsersInfo.Update(info);
            await context.SaveChangesAsync();
        }

        public bool ContainsAny(int telegramUserId, string column, string value)
        {
            // Find duplicate values in database
            try
            {
                int count = context.UsersInfo.FromSql(
                    String.Format(
                        @"SELECT * FROM UsersInfo 
                    WHERE TelegramUserId != '{0}' AND {1} = '{2}'",
                    telegramUserId, column, value)).Count();

                return count > 0 ? true : false;
            }
            catch(Exception e)
            {
                return false;
            }
        }

    }
}
