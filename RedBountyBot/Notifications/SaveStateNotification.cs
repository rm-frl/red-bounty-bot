﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Notifications
{
    public class SaveStateNotification: INotification
    {
        public int TelegramUserId { get; private set; }

        public string CurrentStateClassName { get; private set; }

        public string CurrentDialogClassName { get; private set; }

        public SaveStateNotification(int userId, string state, string dialog)
        {
            TelegramUserId = userId;
            CurrentStateClassName = state;
            CurrentDialogClassName = dialog;
        }
    }
}
