﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Dialogs;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class InitQuestionState : BaseState
    {
        BaseQuestionDialog dialog;

        public InitQuestionState(BaseQuestionDialog dialog) : base(dialog)
        {
            this.dialog = dialog;
        }

        public override void OnEntry()
        {
            dialog.Notify(this, dialog.InitMessage);
            dialog.ChangeState(new WaitQuestionState(dialog));
        }
    }
}
