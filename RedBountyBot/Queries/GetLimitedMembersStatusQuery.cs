﻿using RedBountyBot.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetLimitedMembersStatusQuery: IRequest<bool>
    {
    }

    public class GetLimitedMembersStatusQueryHandler : AsyncRequestHandler<GetLimitedMembersStatusQuery, bool>
    {
        const int LIMIT_MEMBERS_IN_CHAT = 100000;

        IClientService botService;

        public GetLimitedMembersStatusQueryHandler(IClientService botService)
        {
            this.botService = botService;
        }

        protected override async Task<bool> HandleCore(GetLimitedMembersStatusQuery request)
        {
            int count = await botService.Client.GetChatMembersCountAsync(botService.Config.TargetGroupName);
            if (count >= LIMIT_MEMBERS_IN_CHAT)
                return true;
            else
                return false;
        }
    }
}
