﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedBountyBot.DAL
{
    public interface IReferalUserRepository
    {
        int? GetFromId(int referalUserId);
        IEnumerable<ReferalUser> GetAllUserReferals(int telegramUserId);
        bool AddReferal(ReferalUser referal);
        bool RemoveReferal(int referalUserId);
    }
}
