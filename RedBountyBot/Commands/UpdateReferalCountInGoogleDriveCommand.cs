﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateReferalCountInGoogleDriveCommand: IRequest<bool>
    {
        public int FromId { get; private set; }

        public UpdateReferalCountInGoogleDriveCommand(int fromId)
        {
            FromId = fromId;
        }
    }

    public class UpdateReferalCountInGoogleDriveCommandHandler : AsyncRequestHandler<UpdateReferalCountInGoogleDriveCommand, bool>
    {
        IReferalUserRepository referalUserRepository;
        IGoogleDriveService googleDriveService;

        public UpdateReferalCountInGoogleDriveCommandHandler(
            IReferalUserRepository referalUserRepository, IGoogleDriveService googleDriveService)
        {
            this.referalUserRepository = referalUserRepository;
            this.googleDriveService = googleDriveService;
        }

        protected override async Task<bool> HandleCore(UpdateReferalCountInGoogleDriveCommand request)
        {
            await googleDriveService.ExportUserInfo(request.FromId);
            return true;
        }
    }

}
