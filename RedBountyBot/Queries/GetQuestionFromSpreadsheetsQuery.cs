﻿using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetQuestionFromSpreadsheetsQuery: IRequest<RandomQuestion>
    {
        public int RandomIndex { get; private set; }

        public GetQuestionFromSpreadsheetsQuery(int randomIndex)
        {
            RandomIndex = randomIndex;
        }
    }

    public class GetQuestionFromSpreadsheetsQueryHandler : AsyncRequestHandler<GetQuestionFromSpreadsheetsQuery, RandomQuestion>
    {
        IGoogleDriveService googleDriveService;

        public GetQuestionFromSpreadsheetsQueryHandler(IGoogleDriveService googleDriveService)
        {
            this.googleDriveService = googleDriveService;
        }

        protected override Task<RandomQuestion> HandleCore(GetQuestionFromSpreadsheetsQuery request)
        {
            return googleDriveService.GetQuestion(request.RandomIndex);
        }
    }
}
