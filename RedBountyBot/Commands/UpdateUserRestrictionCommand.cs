﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class UpdateUserRestrictionCommand: IRequest
    {
        public int TelegramUserId { get; private set; }
        public bool IsRestricted { get; private set; }

        public UpdateUserRestrictionCommand(int telegramUserId, bool isRestricted)
        {
            TelegramUserId = telegramUserId;
            IsRestricted = isRestricted;
        }
    }

    public class UpdateUserRestrictionCommandHandler : AsyncRequestHandler<UpdateUserRestrictionCommand>
    {
        IUserRepository userRepository;
        public UpdateUserRestrictionCommandHandler(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        protected override async Task HandleCore(UpdateUserRestrictionCommand request)
        {
            try
            {
                var user = userRepository.GetUser(request.TelegramUserId);
                user.IsRestricted = request.IsRestricted;
                userRepository.UpdateUser(user);
            }
            catch(Exception e)
            {

            }
        }
    }
}
