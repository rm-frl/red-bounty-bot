﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class WaitMenuShowDetailsState: BaseState
    {
        public WaitMenuShowDetailsState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void HandleMessage(Update update)
        {
            switch (update.Message.Text)
            {
                case ShowDetailsDialog.SHOW_MY_PROFILE_LABEL:
                    DialogFactory.CreateMyProfileDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case ShowDetailsDialog.SHOW_GROUP_INVITERS:
                    DialogFactory.CreateGroupInvitersDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case ShowDetailsDialog.SHOW_EARNED_POINTS:
                    DialogFactory.CreateEarnedPointsDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case BaseDialog.RETURN_BACK_LABEL:
                    DialogFactory.CreateMainMenuDialog(Dialog.Mediator, typeof(ShowMainMenuState).AssemblyQualifiedName, Dialog.TelegramUserId);
                    break;
            }
        }
    }
}
