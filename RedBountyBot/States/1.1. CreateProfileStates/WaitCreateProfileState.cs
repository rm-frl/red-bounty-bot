﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class WaitCreateProfileState: BaseState
    {
        public WaitCreateProfileState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void HandleMessage(Update update)
        {
            switch (update.Message.Text)
            {
                case CreateProfileDialog.EMAIL_LABEL:
                    DialogFactory.CreateEmailDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case CreateProfileDialog.TWITTER_LABEL:
                    DialogFactory.CreateTwitterDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case CreateProfileDialog.FACEBOOK_LABEL:
                    DialogFactory.CreateFacebookDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case CreateProfileDialog.ETHEREUM_LABEL:
                    DialogFactory.CreateEthereumDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                    break;
                case CreateProfileDialog.RETURN_BACK_LABEL:
                    DialogFactory.CreateMainMenuDialog(Dialog.Mediator, typeof(ShowMainMenuState).AssemblyQualifiedName, Dialog.TelegramUserId);
                    break;

            }
        }
    }
}
