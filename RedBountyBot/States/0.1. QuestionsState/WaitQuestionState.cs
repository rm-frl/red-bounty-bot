﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Commands;
using RedBountyBot.Dialogs;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class WaitQuestionState : BaseState
    {
        BaseQuestionDialog dialog;

        public WaitQuestionState(BaseQuestionDialog dialog) : base(dialog)
        {
            this.dialog = dialog;
        }

        public override void HandleMessage(Update update)
        {
            if (dialog.IsValidMessage(update))
            {
                dialog.SaveSuccessMessage(update);
                dialog.ChangeState(new SuccessQuestionState(dialog));
            }
            else
            {
                int attempts = dialog.NumberAttempts - 1;

                if (attempts > 0)
                {
                    var updateQuestionInfoCommand = new UpdateQuestionInfoCommand(dialog.TelegramUserId)
                    {
                        NumberAttempts = attempts
                    };
                    dialog.Mediator.Send(updateQuestionInfoCommand);
                    dialog.ChangeState(new ErrorQuestionState(dialog));
                }
                else
                {
                    var kickAndBanCommand = new KickAndBanMemberFromChatCommand(dialog.TelegramUserId);
                    dialog.Mediator.Send(kickAndBanCommand);

                    var updateRestrictionModeCommand = new UpdateUserRestrictionCommand(dialog.TelegramUserId, true);
                    dialog.Mediator.Send(updateRestrictionModeCommand);

                    var updateGoogleSpeadsheetsCommand = new UpdateUserInfoInGoogleSpreadsheetsCommand(dialog.TelegramUserId);
                    dialog.Mediator.Send(updateGoogleSpeadsheetsCommand);
                }
            }
        }
    }
}
