﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RedBountyBot.Commands;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using RedBountyBot.States;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.Dialogs
{
    public class QuestionRandomDialog : BaseQuestionDialog
    {
        char splitIndexes = '|';
        TelegramUserInfo userInfo;
        int[] selectedIndexes;
        int NumberAnsweredQuestions;
        const int countQuestionsAccepted = 3;

        public QuestionRandomDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, currentState, telegramUserId)
        {
            NumberAttempts = 3;

            userInfo = Mediator.Send(new GetAllUserInfoQuery(telegramUserId)).Result;
            if (userInfo?.LastQuestionClassName == null ||
                userInfo.LastQuestionClassName != this.GetType().AssemblyQualifiedName)
            {
                userInfo.LastQuestionClassName = this.GetType().AssemblyQualifiedName;
                userInfo.NumberAttempts = NumberAttempts;

                var updateQuestionInfoCommand = new UpdateQuestionInfoCommand(telegramUserId)
                {
                    LastQuestionClassName = userInfo.LastQuestionClassName,
                    NumberAttempts = userInfo.NumberAttempts
                };
                Mediator.Send(updateQuestionInfoCommand);
            }
            else
            {
                NumberAnsweredQuestions = userInfo.NumberAnsweredQuestions;
                NumberAttempts = userInfo.NumberAttempts;
            }
        }

        public override ReplyTextMessage InitMessage => GetInitMessage();
        public override ReplyTextMessage ErrorMessage => GetErrorMessageMessage();
        public override int NumberAttempts { get; protected set; }
        public RandomQuestion RandomQuestion { get; protected set; }

        private readonly string AfterAllSuccess = "Thank you! Now we can start the bounty proper";
        private readonly string Error = "You are left with {0} more chances left, please answer the questions correctly if not you will be banned";

        public override void CallNextDialog()
        {
            userInfo.NumberAnsweredQuestions++;

            if (userInfo.NumberAnsweredQuestions >= countQuestionsAccepted)
            {
                Notify(this, GetAfterAllSuccessMessage());

                DialogFactory.CreateQ1Dialog(Mediator, null, TelegramUserId);

                var updateGoogleSpeadsheetsCommand = new UpdateUserInfoInGoogleSpreadsheetsCommand(TelegramUserId);
                Mediator.Send(updateGoogleSpeadsheetsCommand);
            }
            else
            {
                var updateQuestionInfoCommand = new UpdateQuestionInfoCommand(TelegramUserId)
                {
                    NumberAnsweredQuestions = userInfo.NumberAnsweredQuestions
                };
                Mediator.Send(updateQuestionInfoCommand);
                ChangeState(new InitQuestionState(this));
            }
        }

        

        public override void ChangeState(IState newState)
        {
            if(newState is InitQuestionState)
            {
                GenerateNewQuestion();
            }
            else if(newState is ErrorQuestionState)
            {
                Notify(this, ErrorMessage);
                newState = new InitQuestionState(this);
                this.ChangeState(newState);
                return;
            }
            base.ChangeState(newState);
        }

        public override bool IsValidMessage(Update update)
        {
            bool isValidMessage = false;
            try
            {
                int lastQuestionIndex = GetLastQuestionIndex();

                RandomQuestion question = Mediator.Send(new GetQuestionFromSpreadsheetsQuery(lastQuestionIndex)).Result;

                if(question == null)
                {
                    return false;
                }

                string rightAnswer = question.Answers[question.RightAnswerIndex];

                if (update?.Message?.Text != null)
                {
                    if (update.Message.Type == MessageType.TextMessage && 
                        update.Message.Text.ToLower() == rightAnswer.ToLower())
                    {
                        isValidMessage = true;
                    }
                }
            }
            catch(Exception e) { }

            return isValidMessage;
        }

        public override void SaveSuccessMessage(Update successMessage)
        {
        }

        private ReplyTextMessage GetInitMessage()
        {
            int lastQuestionIndex = GetLastQuestionIndex();

            RandomQuestion question = Mediator.Send(new GetQuestionFromSpreadsheetsQuery(lastQuestionIndex)).Result;

            while(question == null || question.Title == null)
            {
                lastQuestionIndex = GenerateNewQuestion();
                question = Mediator.Send(new GetQuestionFromSpreadsheetsQuery(lastQuestionIndex)).Result;
            }

            var keyboardButtons = new List<KeyboardButton[]>();

            foreach (var answer in question.Answers)
            {
                keyboardButtons.Add(new KeyboardButton[]
                {
                    new KeyboardButton(answer)
                });
            }

            var keyboard = new ReplyKeyboardMarkup();
            keyboard.ResizeKeyboard = true;
            keyboard.OneTimeKeyboard = true;
            keyboard.Keyboard = keyboardButtons.ToArray();

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = question.Title } },
                ReplyMarkup = keyboard
            };
            return reply;
        }

        private ReplyTextMessage GetErrorMessageMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = String.Format(Error, NumberAttempts - 1) } },
            };
            return reply;
        }

        private ReplyTextMessage GetAfterAllSuccessMessage()
        {
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = AfterAllSuccess }, }
            };
            return reply;
        }


        private int GetLastQuestionIndex()
        {
            try
            {
                userInfo = Mediator.Send(new GetAllUserInfoQuery(TelegramUserId)).Result;

                var indexesArray = userInfo.RandomQuestionIndexes.Split(splitIndexes)
                    .Where(s=>!String.IsNullOrEmpty(s))
                    .Select(str => int.Parse(str))
                    .ToList();

                if (indexesArray.Count > 0)
                    return indexesArray.Last();
                else
                    return -1;
            }
            catch(Exception e) { return -1; }
        }

        private int GenerateNewQuestion()
        {
            int newRandomIndex = GetNewRandomIndex();

            string randomQuestionsIndexes = userInfo.RandomQuestionIndexes;
            string needAdd = newRandomIndex.ToString() + splitIndexes;

            if (randomQuestionsIndexes != null)
                randomQuestionsIndexes += needAdd;
            else
                randomQuestionsIndexes = needAdd;

            var updateQuestionInfoCommand = new UpdateQuestionInfoCommand(TelegramUserId)
            {
                RandomQuestionIndexes = randomQuestionsIndexes
            };

            Mediator.Send(updateQuestionInfoCommand);

            return newRandomIndex;
        }

        private int GetNewRandomIndex()
        {
            Random rnd = new Random();

            try
            {
                int newIndex = 0;
                var getAllInfoQuery = new GetAllUserInfoQuery(TelegramUserId);
                userInfo = Mediator.Send(getAllInfoQuery).Result;

                int countQuestions = Mediator.Send(new GetCountQuestionFromSpreadsheetsQuery()).Result;

                if (countQuestions == -1)
                    countQuestions = 50;

                if (userInfo != null && userInfo.RandomQuestionIndexes != null)
                {
                    string[] indexesWithStatus = userInfo.RandomQuestionIndexes.Split(splitIndexes);
                    IEnumerable<int> indexes = indexesWithStatus.Where(s=>!String.IsNullOrEmpty(s)).Select((str) =>
                    {
                        string number = str[0].ToString();
                        return int.Parse(number);
                    });

                    do
                    {
                        newIndex = rnd.Next(0, countQuestions);
                    }
                    while (indexes.Contains(newIndex));
                }
                else
                {
                    newIndex = rnd.Next(0, countQuestions);
                }

                return newIndex;
            }
            catch (Exception e)
            {
                return rnd.Next(0, 10);
            }
        }

    }
}
