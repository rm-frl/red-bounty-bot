﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Interfaces
{
    public interface IGoogleDriveService
    {
        Task ExportUserInfo(int telegramUserId);
        Task<bool> ExportImageToGoogleDrive(int telegramUserId, string columnName, Stream streamFile);
        Task UpdateTwitterStatusesInSpreadsheets();
        Task UpdateTotalPointsInSpreadsheets();
        Task<RandomQuestion> GetQuestion(int id);
        Task<int> GetCountQuestion();
    }
}
