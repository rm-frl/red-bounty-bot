﻿using MediatR;
using RedBountyBot.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Commands
{
    public class RemoveReferalCommand : IRequest<bool>
    {
        public int ReferalId { get; set; }

        public RemoveReferalCommand(int referalId)
        {
            ReferalId = referalId;
        }
    }

    public class RemoveReferalCommandHandler : AsyncRequestHandler<RemoveReferalCommand, bool>
    {
        IReferalUserRepository refUserRepository;

        public RemoveReferalCommandHandler(IReferalUserRepository userRepository)
        {
            this.refUserRepository = userRepository;
        }

        protected override async Task<bool> HandleCore(RemoveReferalCommand request)
        {
            return refUserRepository.RemoveReferal(request.ReferalId);
        }
    }

}
