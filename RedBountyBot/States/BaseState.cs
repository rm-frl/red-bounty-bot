﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using Telegram.Bot.Types;

namespace RedBountyBot.States
{
    public class BaseState : IState
    {
        public BaseState(BaseDialog dialog)
        {
            Dialog = dialog;
        }

        protected BaseDialog Dialog { get; }

        public virtual ReplyTextMessage GetReply() { return null; }

        public virtual void HandleMessage(Update update) { }

        public virtual void OnEntry() { }

        public virtual void OnExit() { }

    }
}
