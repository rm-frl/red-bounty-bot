﻿using RedBountyBot.Interfaces;
using CoreTweet;
using CoreTweet.Core;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace RedBountyBot.Services
{
    public class TwitterService: ITwitterService
    {
        private const string TWITTER_TOKEN_CACHE_KEY = "twitter_token";
        public string TargetGroupName { get; }
        public OAuth2Token Token { get; private set; }

        public TwitterService(IOptions<TwitterConfiguration> config, IMemoryCache cache)
        {
            TargetGroupName = config.Value.TargetGroupName;
            string consumerKey = config.Value.ConsumerKey;
            string consumerSecret = config.Value.ConsumerSecret;

            OAuth2Token token = null;

            if(!cache.TryGetValue(TWITTER_TOKEN_CACHE_KEY, out token))
            {
                try
                {
                    token = OAuth2.GetTokenAsync(consumerKey, consumerSecret).Result;
                    cache.Set(TWITTER_TOKEN_CACHE_KEY, token, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromDays(1)));
                }
                catch (Exception e)
                {

                }
            }

            Token = token;
        }

        public bool IsFollowUser(string screenName, string targetScreenName)
        {
            if (Token == null)
                return false;

            if (screenName.StartsWith('@'))
            {
                screenName = screenName.Substring(1);
            }
            try
            {
                var relation = Token.Friendships.ShowAsync(screenName, targetScreenName).Result;
                return relation.Source.IsFollowing && relation.Target.IsFollowedBy;
            }
            catch
            {
                return false;
            }
        }

        public long? GetUserId(string screenName)
        {
            if (Token == null)
                return null;

            try
            {
                if (screenName.StartsWith('@'))
                {
                    screenName = screenName.Substring(1);
                }
                var response = Token.Users.ShowAsync(screenName).Result;
                return response.Id;
            }
            catch
            {
                return null;
            }
        }

        //public async Task<List<Tuple<int, string, bool>>> FriendshipsLookup(List<Tuple<int, string>> twitterUserNames)
        //{
        //    if (Token == null)
        //        return null;

        //    var tuple = new List<Tuple<int, string, bool>>();
        //    List<long> listIds = new List<long>();
        //    long cursor;
        //    do
        //    {
        //        var response = await Token.Followers.IdsAsync(TargetGroupName);
        //        listIds.AddRange(response);
        //        cursor = response.NextCursor;
        //    } while (cursor != 0);

        //    foreach (var screenName in twitterUserNames)
        //    {
        //        bool existInList = false;
        //        try
        //        {
        //            var response = await Token.Users.ShowAsync(screenName.Item2);
        //            long id = response.Id ?? -1;
        //            if (id != null && id != -1)
        //            {
        //                existInList = listIds.Contains(id);
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        tuple.Add(new Tuple<int, string, bool>(screenName.Item1, screenName.Item2, existInList));
        //    }

        //    return tuple;
        //}
    }
}
