﻿using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedBountyBot.DAL
{
    public interface IUserInfoRepository
    {
        List<TelegramUserInfo> GetAllUsersInfo();

        Task<TelegramUserInfo> GetInfoAsync(int telegramUserId);

        TelegramUserInfo GetInfo(int telegramUserId);

        void CreateInfo(TelegramUserInfo info);

        void UpdateInfo(TelegramUserInfo info);

        Task UpdateInfoAsync(TelegramUserInfo info);

        void RemoveInfo(int telegramUserId);

        bool ContainsAny(int telegramUserId, string column, string value);

    }
}
