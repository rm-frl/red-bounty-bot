﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using RedBountyBot.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace RedBountyBot.States
{
    public class InitGroupInvitersState: BaseState
    {
        public InitGroupInvitersState(BaseDialog Dialog) : base(Dialog)
        {
        }

        public override void OnEntry()
        {
            Dialog.Notify(this, GetReply());
            Dialog.ChangeState(new WaitGroupInvitersState(Dialog));
        }

        public ReplyTextMessage GetReply()
        {
            int countReferals = Dialog.Mediator.Send(new GetReferalsCountQuery(Dialog.TelegramUserId)).Result;
            string text = String.Format("*Members Invites*\n"
                + (countReferals > 0 ? String.Format("Congratulations! You have invited {0} members to the group! Well done!", countReferals) 
                : "Go now and invite more members to gain the bounty program!"));

            var keyboard = new ReplyKeyboardMarkup();
            keyboard.OneTimeKeyboard = true;
            keyboard.ResizeKeyboard = true;
            keyboard.Keyboard = new KeyboardButton[][]
            {
                  new KeyboardButton[]{
                    new KeyboardButton(BaseDialog.RETURN_BACK_LABEL),
                }
            };

            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                ReplyMarkup = keyboard
            };
            return reply;
        }
    }
}
