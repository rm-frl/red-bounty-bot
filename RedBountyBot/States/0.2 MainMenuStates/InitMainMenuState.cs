﻿using RedBountyBot.Dialogs;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.Enums;
using RedBountyBot.Queries;
using RedBountyBot.Commands;

namespace RedBountyBot.States
{
    public class InitMainMenuState: BaseState
    {
        string groupName;
        TelegramUserInfo userInfo;

        public InitMainMenuState(BaseDialog Dialog) : base(Dialog)
        {
            CheckMemberInGroupQuery query = new CheckMemberInGroupQuery(Dialog.TelegramUserId);
            groupName = Dialog.Mediator.Send(query).Result; // return groupName if true, else null

            GetAllUserInfoQuery userInfoQuery = new GetAllUserInfoQuery(Dialog.TelegramUserId);
            userInfo = Dialog.Mediator.Send(userInfoQuery).Result;
        }

        public override void OnEntry()
        {
            if (groupName != null)
            {
                if (userInfo != null && userInfo.IsAllQuestionsAnswered)
                {
                    Dialog.ChangeState(new WelcomeMainMenuState(Dialog));
                }
                else
                {
                    Dialog.Notify(this, GetReplyQuestionInstruction());

                    if (userInfo == null)
                    {
                        var userInfo = new TelegramUserInfo() { TelegramUserId = Dialog.TelegramUserId };
                        Dialog.Mediator.Send(new CreateUserInfoCommand(userInfo));
                    }
                    DialogFactory.CreateRandomDialog(Dialog.Mediator, null, Dialog.TelegramUserId);
                }
            }
            else
            {
                Dialog.Notify(this, GetReply());
            }
        }

        public ReplyTextMessage GetReply()
        {
            string text = "Hi, welcome to Real Estate Doc Bounty and Referral Program bot! Start earning tokens by joining the @RealEstateDoc / https://t.me/RealEstateDoc Telegram group!";
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                DisableWebPageView = false
            };
            return reply;
        }

        public ReplyTextMessage GetReplyQuestionInstruction()
        {
            string text = "Thank you for initiating the Real Estate Doc Bounty process! We appreciate your interest. Please answer the next few questions to the best of your abilities - 3 correct answers are required. Too many wrong answers will result in a ban.";
            var reply = new ReplyTextMessage()
            {
                Update = new Update() { Message = new Message() { Text = text } },
                DisableWebPageView = false
            };
            return reply;
        }
    }
}
