﻿using RedBountyBot.States;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Dialogs
{
    public class EarnedPointsDialog: BaseDialog
    {
        public EarnedPointsDialog(IMediator mediator, string currentState, int telegramUserId) : base(mediator, telegramUserId)
        {
            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitEarnedPointsState(this));
            }
        }
    }
}
