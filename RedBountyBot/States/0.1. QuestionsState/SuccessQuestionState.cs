﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RedBountyBot.Dialogs;

namespace RedBountyBot.States
{
    public class SuccessQuestionState : BaseState
    {
        BaseQuestionDialog dialog;
        public SuccessQuestionState(BaseQuestionDialog dialog) : base(dialog)
        {
            this.dialog = dialog;
        }

        public override void OnEntry()
        {
            dialog.CallNextDialog();
        }
    }
}
