﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using RedBountyBot.Commands;

namespace RedBountyBot.Controllers
{
    public class MessagesController : Controller
    {
        IMediator mediator;
        IClientService botService;

        public MessagesController(IMediator mediator, IClientService botService)
        {
            this.mediator = mediator;
            this.botService = botService;
        }

        [HttpGet]
        public IEnumerable<string> Index()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        public async Task<IActionResult> SetWebHook(string url)
        {
            try
            {
                botService.Client.SetWebhookAsync(url + "/messages/apply").Wait();
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet]
        public async Task<string> UpdateTwitterFollowers()
        {
            var updateAllTwitterStatuses = new UpdateAllTwitterStatusesInDatabaseCommand();
            mediator.Send(updateAllTwitterStatuses);

            var updateTwitterStatusCommand = new UpdateTwitterStatusesInSpreadsheetsCommand();
            mediator.Send(updateTwitterStatusCommand);
            return "Twitter statuses in spreadsheets has updated!";
        }

        [HttpGet]
        public async Task<string> UpdateTotalPoints()
        {
            var updateTotalPointsInDatabaseCommand = new UpdateTotalPointsInDatabaseCommand();
            mediator.Send(updateTotalPointsInDatabaseCommand);

            var updateTotalPointsInSpreadsheetsCommand = new UpdateTotalPointsInSpreadsheetsCommand();
            mediator.Send(updateTotalPointsInSpreadsheetsCommand);

            return "Total points in spreadsheets has updated!";
        }

        [HttpGet]
        public async Task<IActionResult> StopService(string url)
        {
            try
            {
                botService.Client.SetWebhookAsync(url + "/messages/DontApply").Wait();
                return Ok();
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> DontApply([FromBody]Update update)
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Apply([FromBody]Update update)
        {
            if (update != null)
            {
                if (update.Message != null)
                {
                    try
                    {
                        ParseMessageCommand command = new ParseMessageCommand() { Update = update };
                        mediator.Send(command).Wait();
                    }
                    catch (Exception e)
                    {
                        return NotFound();
                    }
                }
            }
            return Ok();
        }
    }
}