﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Interfaces;
using RedBountyBot.Models;
using RedBountyBot.Notifications;
using RedBountyBot.Queries;
using RedBountyBot.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace RedBountyBot.Commands
{
    public class ParseMessageCommand: IRequest<bool>
    {
        public Update Update { get; set; }
    }

    public class ParseMessageCommandHandler : AsyncRequestHandler<ParseMessageCommand, bool>
    {
        IMediator mediator;
        IDialogService dialogService;
        IUserRepository userRepository;
        IUserInfoRepository userInfoRepository;
        IReferalUserRepository refRepository;

        public ParseMessageCommandHandler(IMediator mediator, IUserInfoRepository userInfoRepository, IUserRepository userRepository, IDialogService dialogService, IReferalUserRepository refRepository)
        {
            this.mediator = mediator;
            this.dialogService = dialogService;
            this.userRepository = userRepository;
            this.userInfoRepository = userInfoRepository;
            this.refRepository = refRepository;
        }

        protected override async Task<bool> HandleCore(ParseMessageCommand request)
        {
            //var getLimitedMembersStatusQuery = new GetLimitedMembersStatusQuery();
            //bool membersReachedLimit = mediator.Send(getLimitedMembersStatusQuery).Result; // true - if members reached limit 20k in spreadsheets 
            bool membersReachedLimit = false; 

            try
            {
                if (request?.Update?.Message?.From?.Id != null)
                {
                    var getRestrictedStatusQuery = new GetUserRestrictionQuery(request.Update.Message.From.Id);
                    bool isRestrictedUser = mediator.Send(getRestrictedStatusQuery).Result;
                    if (isRestrictedUser)
                        return false;
                }
            }
            catch (Exception e) { return false; }

            if (request.Update.Message.Type != MessageType.ServiceMessage &&
                request.Update.Message.Chat.Type == ChatType.Private)
            {
                var user = userRepository.GetUser(request.Update.Message.From.Id);
                if (user == null)
                {
                    if (membersReachedLimit) // if user not registered before, that bot will not responsing
                        return true;

                    user = new TelegramUser()
                    {
                        Id = request.Update.Message.From.Id,
                        ChatId = request.Update.Message.Chat.Id,
                        FirstName = request.Update.Message.From.FirstName,
                        LastName = request.Update.Message.From.LastName,
                        UserName = request.Update.Message.From.Username,
                    };
                    var createUserNotification = new CreateUserNotification(user);
                    mediator.Publish(createUserNotification);
                }

                var userInfo = userInfoRepository.GetInfo(request.Update.Message.From.Id);
                if (request.Update.Message.Text == "/start")
                {
                    user.CurrentDialogClassName = typeof(Dialogs.MainMenuDialog).AssemblyQualifiedName;
                    user.CurrentStateClassName = null;

                    if (userInfo != null)
                    {
                        if (!userInfo.IsAllQuestionsAnswered && userInfo.LastQuestionClassName != null)
                        {
                            user.CurrentDialogClassName = userInfo.LastQuestionClassName;
                            user.CurrentStateClassName = typeof(WaitQuestionState).AssemblyQualifiedName;
                        }
                    }
                }

                return dialogService.SendReplyToUser(request.Update, user);
            }
            else if (
                request.Update.Message.Type == MessageType.ServiceMessage &&
                request.Update.Message.From != null &&
                request.Update.Message.NewChatMember != null &&
                request.Update.Message.NewChatMembers != null &&
                request.Update.Message.NewChatMembers.Length > 0 &&
                request.Update.Message.From.Id != request.Update.Message.NewChatMember.Id &&
                !request.Update.Message.NewChatMember.IsBot)
            {
                if (membersReachedLimit)
                    return true;

                var user = userRepository.GetUser(request.Update.Message.From.Id);
                if (user == null)
                {
                    user = new TelegramUser()
                    {
                        Id = request.Update.Message.From.Id,
                        ChatId = request.Update.Message.From.Id,
                        FirstName = request.Update.Message.From.FirstName,
                        LastName = request.Update.Message.From.LastName,
                        UserName = request.Update.Message.From.Username,
                    };
                    var createUserNotification = new CreateUserNotification(user);
                    mediator.Publish(createUserNotification);
                }

                List<ReferalUser> referals = new List<ReferalUser>();

                foreach (var r in request.Update.Message.NewChatMembers)
                {
                    var newReferal = new ReferalUser();

                    newReferal.ReferalUserId = r.Id;

                    if (r.Username != null) newReferal.UserName = r.Username;
                    if (r.FirstName != null) newReferal.FirstName = r.FirstName;
                    if (r.LastName != null) newReferal.LastName = r.LastName;
                    referals.Add(newReferal);
                }

                var saveReferalsCommand = new SaveReferalsCommand(request.Update.Message.From.Id, referals);
                mediator.Send(saveReferalsCommand);

                var calculatePointsCommand = new CalculatePointsCommand(request.Update.Message.From.Id);
                mediator.Send(calculatePointsCommand);

                var updateReferalsCommand = new UpdateReferalCountInGoogleDriveCommand(request.Update.Message.From.Id);
                mediator.Send(updateReferalsCommand);

                //foreach (var r in referals)
                //{
                //    UpdateReferalCountInGoogleDriveCommand updateReferalsCommand = new UpdateReferalCountInGoogleDriveCommand(r.ReferalUserId);
                //    mediator.Send(updateReferalsCommand);
                //}

                return true;
            }
            else if (request.Update.Message.Type == MessageType.ServiceMessage &&
                 request.Update.Message.LeftChatMember != null &&
                 !request.Update.Message.LeftChatMember.IsBot)
            {
                int telegramUserId = refRepository.GetFromId(request.Update.Message.LeftChatMember.Id) ?? -1;

                RemoveReferalCommand removeReferalCommand = new RemoveReferalCommand(request.Update.Message.LeftChatMember.Id);
                mediator.Send(removeReferalCommand).Wait();

                if (telegramUserId != -1)
                {
                    CalculatePointsCommand calculatePointsCommand = new CalculatePointsCommand(request.Update.Message.From.Id);
                    mediator.Send(calculatePointsCommand).Wait();

                    UpdateReferalCountInGoogleDriveCommand updateReferalsCommand = new UpdateReferalCountInGoogleDriveCommand(telegramUserId);
                    mediator.Send(updateReferalsCommand).Wait();
                }

                return true;
            }
            else if (
              request.Update.Message.NewChatMembers != null &&
              request.Update.Message.NewChatMembers.Length > 0)
            {
                if (membersReachedLimit)
                    return true;

                var newMembers = request.Update.Message.NewChatMembers;
                foreach (var member in newMembers)
                {
                    var user = userRepository.GetUser(member.Id);
                    if (user != null)
                    {
                        dialogService.SendReplyToUser(request.Update, user);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
