﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RedBountyBot.Commands;
using RedBountyBot.Models;
using RedBountyBot.Notifications;
using RedBountyBot.States;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace RedBountyBot.Dialogs
{
    public abstract class BaseQuestionDialog : BaseDialog
    {
        public BaseQuestionDialog(IMediator mediator, string currentState, int telegramUserId, string name = "") : base(mediator, telegramUserId)
        {
            Name = name;

            if (currentState != null)
            {
                var stateType = Type.GetType(currentState);

                if (stateType != null)
                {
                    CurrentState = (Activator.CreateInstance(stateType, this) as BaseState);
                    return;
                }
            }
            else
            {
                ChangeState(new InitQuestionState(this));
            }
        }

        public abstract void CallNextDialog();

        public virtual ReplyTextMessage InitMessage { get; protected set; }

        public virtual ReplyTextMessage ErrorMessage { get; protected set; }

        public virtual int NumberAttempts { get; protected set; }

        public virtual void SaveSuccessMessage(Update successMessage)
        {
            if (!String.IsNullOrEmpty(Name))
            {
                var saveMessageNotification = new SaveSuccessMessageNotification(Name, successMessage);
                Mediator.Publish(saveMessageNotification);

                var updateInfoInSpreadsheetsCommand = new UpdateUserInfoInGoogleSpreadsheetsCommand(TelegramUserId);
                Mediator.Send(updateInfoInSpreadsheetsCommand);
            }
        }

        public override bool IsValidMessage(Update update)
        {
            bool isValidMessage = false;

            try
            {
                if (update?.Message?.Text != null)
                {
                    if (update.Message.Type == MessageType.TextMessage)
                    {
                        isValidMessage = true;
                    }
                }
            }
            catch (Exception e) { }

            return isValidMessage;
        }
    }
}
