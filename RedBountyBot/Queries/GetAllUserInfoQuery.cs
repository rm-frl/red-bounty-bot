﻿using MediatR;
using RedBountyBot.DAL;
using RedBountyBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedBountyBot.Queries
{
    public class GetAllUserInfoQuery: IRequest<TelegramUserInfo>
    {
        public int TelegramUserId { get; private set; }

        public GetAllUserInfoQuery(int telegramUserId)
        {
            TelegramUserId = telegramUserId;
        }
    }

    public class GetAllUserInfoQueryHandler : AsyncRequestHandler<GetAllUserInfoQuery, TelegramUserInfo>
    {
        IUserInfoRepository userInfoRepository;

        public GetAllUserInfoQueryHandler(IUserInfoRepository userInfoRepository)
        {
            this.userInfoRepository = userInfoRepository;
        }

        protected override async Task<TelegramUserInfo> HandleCore(GetAllUserInfoQuery request)
        {
            var userInfo = await userInfoRepository.GetInfoAsync(request.TelegramUserId);
            return userInfo;
        }
    }
}
